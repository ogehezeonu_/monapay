<?php
$mainpath = "/";
?>
    <!doctype html>
    <html lang="en">

    <head>
        <!-- COMMON TAGS -->
        <meta charset="utf-8">
        <title>About Us - Airtime for Payments</title>
        <!-- Search Engine -->
        <meta name="description" content="What is Monapay - A Payment Gateway designed for Digital Content Providers to enable Airtime Payments in Nigeria.">
        <!-- Schema.org for Google -->
        <meta itemprop="name" content="About Us - Airtime for Payments">
        <meta itemprop="description" content="What is Monapay - A Payment Gateway designed for Digital Content Providers to enable Airtime Payments in Nigeria.">
        <!-- Twitter -->
        <meta name="twitter:card" content="summary">
        <meta name="twitter:title" content="About Us - Airtime for Payments">
        <meta name="twitter:description" content="What is Monapay - A Payment Gateway designed for Digital Content Providers to enable Airtime Payments in Nigeria.">
        <!-- Open Graph general (Facebook, Pinterest & Google+) -->
        <meta name="og:title" content="About Us - Airtime for Payments">
        <meta name="og:description" content="What is Monapay - A Payment Gateway designed for Digital Content Providers to enable Airtime Payments in Nigeria.">
        <meta name="og:image" content="https://monapay.com/img/opengraph.png">
        <meta name="og:url" content="https://monapay.com">
        <meta name="og:site_name" content="Monapay Site">
        <meta name="og:type" content="website">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/stack-interface.css" rel="stylesheet" type="text/css" media="all">
        <link href="css/socicon.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/flickity.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet" type="text/css">
        <link href="css/font-roboto.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="css/devices.min.css" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="css/index-codepanel.css">
        <link rel="stylesheet" href="css/default-codepanel.css">
        <link rel="stylesheet" href="css/prism_light.css">
        <script defer src="js/default-codepanel.js"></script>
        <script defer src="js/index-codepanel.js"></script>
        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="img/Monapay_favicon.png">
        <link rel="icon" type="image/png" href="img/Monapay_favicon.png" sizes="32x32">
    </head>
    <style type="text/css">
        .hide-it {
            display: none !important;
        }
    </style>

<body class="scroll-assist" data-smooth-scroll-offset="77">
    <a id="start"></a>
    <div class="nav-container">
        <div class="via-1491986892298" via="via-1491986892298" vio="monapay">
            <div class="bar bar--sm visible-xs">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-8 col-sm-2">
                            <a href="<?= $mainpath ?>home.php"> <img class="logo logo-dark" alt="logo" src="img/Asset%201monapay%20logo.svg"> <img class="logo logo-light" alt="logo" src="img/Asset%201monapay%20logo.svg"> </a>
                        </div>
                        <div class="col-xs-12 col-sm-5 text-right">
                            <a href="#" class="hamburger-toggle" data-toggle-class="#menu1;hidden-xs hidden-sm"> <i class="material-icons">menu</i> </a>
                        </div>
                    </div>
                </div>
            </div>
            <nav id="menu1" class="bar bar--sm bar-1 hidden-xs" data-scroll-class="10vh:pos-fixed">
                <div class="container">
                    <div class="row">
                        <div class="col-md-2 col-sm-2 hidden-xs header1">
                            <div class="bar__module">
                                <a href="<?= $mainpath ?>home.php"> <img class="logo logo-dark" alt="logo" src="img/Asset%201monapay%20logo.svg"> <img class="logo logo-light" alt="logo" src="img/Asset%201monapay%20logo.svg"> </a>
                            </div>
                        </div>
                        <div class="col-md-11 col-sm-12 text-right text-left-xs text-left-sm header2">
                            <div class="bar__module">
                                <ul class="menu-horizontal text-left">
                                    <li> <a href="<?= $mainpath ?>home.php#how-it-works" class="inner-link" target="_self">
                                            How it Works
                                        </a> </li>
                                    <li> <a href="<?= $mainpath ?>home.php#features" class="inner-link" target="_self">
                                            Features
                                        </a> </li>
                                    <li> <a href="<?= $mainpath ?>home.php#developers" class="inner-link" target="_self">Developers</a>
                                    </li>
                                    <li> <a href="http://www.maliyo.com">
                                            Marketplace
                                        </a> </li>
                                </ul>
                            </div>
                            <div class="bar__module">
                                <div class="modal-instance sgn-up">
                                    <a class="btn btn--sm modal-trigger sign__up" href="#"> <span class="btn__text">
                            Demo Account
                        </span> </a>
                                    <div class="modal-container modal" data-modal-index="4">
                                        <div class="modal-content">
                                            <section class="unpad ">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-sm-6 col-md-offset-3 col-sm-offset-3 col-xs-12">
                                                            <div class="feature feature-1">
                                                                <div class="feature__body boxed boxed--lg boxed--border text-center">
                                                                    <div class="modal-close modal-close-cross"></div>
                                                                    <!-- Demo Account form --->
                                                                    <div>
                                                                        <div class="text-block">
                                                                            <h3>Request for demo account</h3>
                                                                            <p>
                                                                                As a developer, you can request for demo account and testing parameters.
                                                                            </p>
                                                                        </div>
                                                                        <?php include ("requestform.phtml"); ?>
                                                                        <!-- end of demo account form -->
                                                                        <!-- flip forms
                                                                                                <div id="login">
                                                                                                    <div class="text-block">
                                                                                                        <h3>Login to your account</h3>
                                                                                                        <p>
                                                                                                            Welcome back, sign in with your existing account credentials.
                                                                                                        </p>
                                                                                                    </div>
                                                                                                    <form>
                                                                                                        <div class="row">
                                                                                                            <div class="col-sm-12">
                                                                                                                <input type="email" name="Email Address" placeholder="Email Address">
                                                                                                            </div>
                                                                                                            <div class="col-sm-12">
                                                                                                                <input type="password" placeholder="Password">
                                                                                                            </div>
                                                                                                            <div class="col-sm-12">
                                                                                                                <button class="btn btn--primary type--uppercase" type="submit"><span class="btn__text">Login</span></button>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                    </form>
                                                                                                    <span class="type--fine-print block">Don't have an account yet? <a href="#" id="flip-login">Create account</a></span> <span class="type--fine-print block">Forgot your username or password? <a href="#" id="flip-recover">Recover account</a></span>
                                                                                                </div>

                                                                                                <div class="hidden" id="recover-account">
                                                                                                    <div class="row">
                                                                                                        <div class="text-block">
                                                                                                            <h3>Recover your account</h3>
                                                                                                            <p> Enter email address to send a recovery email. </p>
                                                                                                            <form>
                                                                                                                <div class="col-sm-12">
                                                                                                                    <input type="email" placeholder="Email Address"></div>
                                                                                                                <div class="col-sm-12">
                                                                                                                    <button class="btn btn--primary type--uppercase" type="submit"><span class="btn__text">Recover Account</span></button>
                                                                                                                </div>
                                                                                                            </form>
                                                                                                            <span class="type--fine-print block">Dont have an account yet? <a href="#" id="flip-signup-recover">Create account</a></span>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="hidden" id="signup">
                                                                                                    <div class="row">
                                                                                                        <div class="text-block">
                                                                                                            <h3>Sign up and start earning with monapay</h3>
                                                                                                            <p>Get started with monapay today, No credit card required.</p>
                                                                                                            <form>
                                                                                                                <div class="row">
                                                                                                                    <div class="col-sm-12"> <input type="email" name="Email Address" placeholder="Email Address"> </div>
                                                                                                                    <div class="col-sm-12"> <input type="password" name="Password" placeholder="Password"> </div>
                                                                                                                    <div class="col-sm-12"> <button type="submit" class="btn btn--primary type--uppercase"><span class="btn__text">Create Account</span></button> </div>
                                                                                                                    <div class="col-sm-12"> <span class="type--fine-print">By signing up, you agree to the <a href="terms-of-service.html">Terms of Service</a></span> </div>
                                                                                                                </div>
                                                                                                            </form>
                                                                                                            <span class="type--fine-print block">Already a user? <a href="#" id="flip-signup">Login</a></span>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>-->

                                                                    </div>
                                                                </div>
                                                                <!--end feature-->
                                                            </div>
                                                        </div>
                                                        <!--end of row-->
                                                    </div>
                                                    <!--end of container-->
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>
    <div class="main-container">
        <div class="main-content">
            <section class="text-center imagebg space--md" data-overlay="5">
                <div class="background-image-holder"><img alt="background" src="img/about_.jpg"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-10 col-md-8">
                            <h1 class="white-text">About Monapay</h1>

                        </div>
                    </div>
                </div>
            </section>
            <section class="cover space--md unpad--bottom switchable">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-7">
                            <div class="mt--1 old--way">
                                <h3 class="h3">THE OLD WAY</h3>
                                <p class="lead-x">So many lost conversations & enthusiasm as a result of the unfavourable terms and endless bureaucracy associated with airtime payment (watch video). We built Monapay because we believe there is a better way to monetize digital content. </p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-5 col-xs-12"> <img alt="Image" src="img/THE-OLD-WAY.jpg"> </div>
                    </div>
                </div>
            </section>
            <section class="switchable cover unpad--bottom switchable--switch">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-7">
                            <div class="mt--1 new--way">
                                <h3 class="h3">THE NEW WAY</h3>
                                <h5>The Mobile Payment Advantage</h5>
                                <p class="lead-x">Monapay thrives on innovation by introducing a clever payment product that is easier, cheaper and more flexible. Now, the hassle with Direct Carrier Billing is over! You can charge for one-off payments, enable in-app purchases or even create a subscription service through our convenient payment gateway.</p>
                                <p class="lead-x spac--btm">Monapay is committed to making online payments less complicated for everyone. This product has been designed with love for digital creatives. Check out our simple API integration and seamless user-onboarding.
                                    <!-- demo button-->
                                <div class="modal-instance" style="margin-bottom: 11.14285714em">
                                    <a class="btn btn--purple  modal-trigger" href="#"> <span class="btn__text">Try Demo</span> </a>
                                    <div class="modal-container modal" data-modal-index="4">
                                        <div class="modal-content">
                                            <section class="unpad ">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-sm-6 col-md-offset-3 col-sm-offset-3 col-xs-12 iphone--modal">
                                                            <div class="feature feature-1">
                                                                <div class="feature__body boxed boxed--lg boxed--border text-center modal--container--hide">
                                                                    <!-- content goes here-->
                                                                    <div class="marvel-device iphone6 black">
                                                                        <div class="top-bar"></div>
                                                                        <div class="sleep"></div>
                                                                        <div class="volume"></div>
                                                                        <div class="camera"></div>
                                                                        <div class="sensor"></div>
                                                                        <div class="speaker"></div>
                                                                        <div class="screen">
                                                                            <div class="modal-close modal-close-cross" style="top: .5em;"></div>
                                                                            <!-- Content goes here -->
                                                                            <iframe src="<?= $mainpath ?>index-webapp.php" width="100%" height="100%"></iframe>
                                                                        </div>
                                                                        <div class="home"></div>
                                                                        <div class="bottom-bar"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--end feature-->
                                                        </div>
                                                    </div>
                                                    <!--end of row-->
                                                </div>
                                                <!--end of container-->
                                            </section>
                                        </div>
                                    </div>
                                </div>
                                <!-- end of demo button--></p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-5 col-xs-12"> <img alt="Image" src="img/THE-NEW-WAY.jpg"> </div>
                    </div>
                </div>
            </section>
            <a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
                <i class="stack-interface stack-up-open-big"></i>
            </a>
        </div>
        <?php include("footer.php"); ?>