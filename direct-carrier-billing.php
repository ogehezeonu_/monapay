<?php
$mainpath = "/";
?>
    <!doctype html>
    <html lang="en">

    <head>
        <!-- COMMON TAGS -->
        <meta charset="utf-8">
        <title>Direct Carrier Billing - A payment system that works for both prepaid and postpaid customers.</title>
        <!-- Search Engine -->
        <meta name="description" content="Direct carrier billing is a payment system that enables mobile payment through airtime and requires integration with a mobile network operator.">
        <meta name="image" content="https://monapay.com/img/opengraph.png">
        <!-- Schema.org for Google -->
        <meta itemprop="name" content="Direct Carrier Billing - A payment system that works for both prepaid and postpaid customers.">
        <meta itemprop="description" content="Direct carrier billing is a payment system that enables mobile payment through airtime and requires integration with a mobile network operator.">
        <meta itemprop="image" content="https://monapay.com/img/opengraph.png">
        <!-- Open Graph general (Facebook, Pinterest & Google+) -->
        <meta name="og:title" content="Direct Carrier Billing - A payment system that works for both prepaid and postpaid customers.">
        <meta name="og:description" content="Direct carrier billing is a payment system that enables mobile payment through airtime and requires integration with a mobile network operator.">
        <meta name="og:image" content="https://monapay.com/img/opengraph.png">
        <meta name="og:url" content="https://monapay.com">
        <meta name="og:site_name" content="Monapay site">
        <meta name="og:type" content="website">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/stack-interface.css" rel="stylesheet" type="text/css" media="all">
        <link href="css/socicon.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/flickity.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet" type="text/css">
        <link href="css/font-roboto.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="css/devices.min.css" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="css/index-codepanel.css">
        <link rel="stylesheet" href="css/default-codepanel.css">
        <link rel="stylesheet" href="css/prism_light.css">
        <script defer src="js/default-codepanel.js"></script>
        <script defer src="js/index-codepanel.js"></script>
        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="img/Monapay_favicon.png">
        <link rel="icon" type="image/png" href="img/Monapay_favicon.png" sizes="32x32">
    </head>
    <style type="text/css">
        .hide-it {
            display: none !important;
        }

    </style>

    <body class="scroll-assist" data-smooth-scroll-offset="77">
        <a id="start"></a>
        <div class="nav-container">
            <div class="via-1491986892298" via="via-1491986892298" vio="monapay">
                <div class="bar bar--sm visible-xs">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-8 col-sm-2">
                                <a href="<?= $mainpath ?>home.php"> <img class="logo logo-dark" alt="logo" src="img/Asset%201monapay%20logo.svg"> <img class="logo logo-light" alt="logo" src="img/Asset%201monapay%20logo.svg"> </a>
                            </div>
                            <div class="col-xs-12 col-sm-5 text-right">
                                <a href="#" class="hamburger-toggle" data-toggle-class="#menu1;hidden-xs hidden-sm"> <i class="material-icons">menu</i> </a>
                            </div>
                        </div>
                    </div>
                </div>
                <nav id="menu1" class="bar bar--sm bar-1 hidden-xs" data-scroll-class="10vh:pos-fixed">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-2 col-sm-2 hidden-xs header1">
                                <div class="bar__module">
                                    <a href="<?= $mainpath ?>home.php"> <img class="logo logo-dark" alt="logo" src="img/Asset%201monapay%20logo.svg"> <img class="logo logo-light" alt="logo" src="img/Asset%201monapay%20logo.svg"> </a>
                                </div>
                            </div>
                            <div class="col-md-11 col-sm-12 text-right text-left-xs text-left-sm header2">
                                <div class="bar__module">
                                    <ul class="menu-horizontal text-left">
                                        <li> <a href="<?= $mainpath ?>home.php#how-it-works" class="inner-link" target="_self">
                                        How it Works
                                    </a> </li>
                                        <li> <a href="<?= $mainpath ?>home.php#features" class="inner-link" target="_self">
                                        Features
                                    </a> </li>
                                        <li> <a href="<?= $mainpath ?>home.php#developers" class="inner-link" target="_self">Developers</a>
                                        </li>
                                        <li> <a href="<?= $mainpath ?>home.php#marketplace">
                                      Marketplace
                                    </a> </li>
                                    </ul>
                                </div>
                                <div class="bar__module">
                                    <div class="modal-instance sgn-up">
                                        <a class="btn btn--sm modal-trigger sign__up" href="#"> <span class="btn__text">
                            Demo Account
                        </span> </a>
                                        <div class="modal-container modal" data-modal-index="4">
                                            <div class="modal-content">
                                                <section class="unpad ">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-sm-6 col-md-offset-3 col-sm-offset-3 col-xs-12">
                                                                <div class="feature feature-1">
                                                                    <div class="feature__body boxed boxed--lg boxed--border text-center">
                                                                        <div class="modal-close modal-close-cross"></div>
                                                                        <!-- Demo Account form --->
                                                                        <div>
                                                                            <div class="text-block">
                                                                                <h3>Request for demo account</h3>
                                                                                <p>
                                                                                    As a developer, you can request for demo account and testing parameter!
                                                                                </p>
                                                                            </div>
                                                                            <form>
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="text" name="fullname" placeholder="Developer name">
                                                                                    </div>
                                                                                    <div class="col-sm-12">
                                                                                        <input type="email" name="Email Address" placeholder="Email Address">
                                                                                    </div>
                                                                                    <div class="col-sm-12">
                                                                                        <button class="btn btn--primary type--uppercase" type="submit"><span class="btn__text">Submit</span></button>
                                                                                    </div>
                                                                                </div>
                                                                                <!--end of row-->
                                                                            </form>
                                                                        </div>
                                                                        <!-- end of demo account form -->
                                                                        <!-- flip forms
                                                                    <div id="login">
                                                                        <div class="text-block">
                                                                            <h3>Login to your account</h3>
                                                                            <p>
                                                                                Welcome back, sign in with your existing account credentials.
                                                                            </p>
                                                                        </div>
                                                                        <form>
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <input type="email" name="Email Address" placeholder="Email Address">
                                                                                </div>
                                                                                <div class="col-sm-12">
                                                                                    <input type="password" placeholder="Password">
                                                                                </div>
                                                                                <div class="col-sm-12">
                                                                                    <button class="btn btn--primary type--uppercase" type="submit"><span class="btn__text">Login</span></button>
                                                                                </div>
                                                                            </div>

                                                                        </form>
                                                                        <span class="type--fine-print block">Don't have an account yet? <a href="#" id="flip-login">Create account</a></span> <span class="type--fine-print block">Forgot your username or password? <a href="#" id="flip-recover">Recover account</a></span>
                                                                    </div>

                                                                    <div class="hidden" id="recover-account">
                                                                        <div class="row">
                                                                            <div class="text-block">
                                                                                <h3>Recover your account</h3>
                                                                                <p> Enter email address to send a recovery email. </p>
                                                                                <form>
                                                                                    <div class="col-sm-12">
                                                                                        <input type="email" placeholder="Email Address"></div>
                                                                                    <div class="col-sm-12">
                                                                                        <button class="btn btn--primary type--uppercase" type="submit"><span class="btn__text">Recover Account</span></button>
                                                                                    </div>
                                                                                </form>
                                                                                <span class="type--fine-print block">Dont have an account yet? <a href="#" id="flip-signup-recover">Create account</a></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="hidden" id="signup">
                                                                        <div class="row">
                                                                            <div class="text-block">
                                                                                <h3>Sign up and start earning with monapay</h3>
                                                                                <p>Get started with monapay today, No credit card required.</p>
                                                                                <form>
                                                                                    <div class="row">
                                                                                        <div class="col-sm-12"> <input type="email" name="Email Address" placeholder="Email Address"> </div>
                                                                                        <div class="col-sm-12"> <input type="password" name="Password" placeholder="Password"> </div>
                                                                                        <div class="col-sm-12"> <button type="submit" class="btn btn--primary type--uppercase"><span class="btn__text">Create Account</span></button> </div>
                                                                                        <div class="col-sm-12"> <span class="type--fine-print">By signing up, you agree to the <a href="terms-of-service.php">Terms of Service</a></span> </div>
                                                                                    </div>
                                                                                </form>
                                                                                <span class="type--fine-print block">Already a user? <a href="#" id="flip-signup">Login</a></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>-->

                                                                    </div>
                                                                </div>
                                                                <!--end feature-->
                                                            </div>
                                                        </div>
                                                        <!--end of row-->
                                                    </div>
                                                    <!--end of container-->
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
        <div class="main-container">
            <div class="main-content">
                <!-- content of the page goes here -->
                <section class="text-center imagebg space--md" data-overlay="5">
                    <div class="background-image-holder"><img alt="background" src="img/direct-carrier-billing.jpg"></div>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-10 col-md-8">
                                <h1 class="white-text">Direct Carrier Billing (DCB)</h1>
                            </div>
                        </div>
                    </div>
                </section>
                <section>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6 dcb">
                                <h2>What is DCB?</h2>
                                <p class="lead-x"> DCB refers to a payment system that enables mobile payment through airtime and requires integration with a mobile network operator. This method of billing works for both prepaid and post-paid customers.</p>
                                <p class="lead-x">In many emerging markets, people with Smartphones now use Direct Carrier Billing to pay for goods and services in apps and on mobile sites. DCB is also used as a preferred means of payment for votes on various reality TV Talent Shows in Nigeria. </p>
                            </div>
                            <div class="col-sm-6">
                                <div class="boxed boxed--lg boxed--border bg--secondary">
                                    <h2>Benefits of DCB</h2>
                                    <ul>
                                        <li class="lead-x">- Instantaneous payments.</li>
                                        <li class="lead-x">- Protection of customer identity.</li>
                                        <li class="lead-x">- Elimination of fraud.</li>
                                        <li class="lead-x">- Ease of bill payments.</li>
                                        <li class="lead-x">- Lower customer support costs for service providers and merchants.</li>
                                        <li class="lead-x">- Price point flexibility.</li>
                                        <li class="lead-x">- Higher customer reaches than credit cards.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="switchable feature-large switchable--switch bg--grey">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-7 payment">
                                <img alt="Image" class="image-holder" src="img/Pay%20now-game.png"> </div>
                            <div class="col-sm-6 col-md-5">
                                <div class="switchable__text pay">
                                    <h2 class="h3">DCB in Nigeria</h2>
                                    <p class="lead-x"> In Nigeria, to offer DCB as a payment option, service providers need a Value-Added Service (VAS) licence from the Nigerian Communications Commission (NCC) before integration with Mobile Networks such as MTN, Airtel, Glo and Etisalat is possible. </p>
                                    <a href="#"> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="switchable feature-large white">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-7 dcb-content">
                                <img alt="Image" class="image-holder dcb" src="img/Mobile-mockup.png"> </div>
                            <div class="col-sm-6 col-md-5">
                                <div class="switchable__text pay">
                                    <h2 class="h3">What types of content can I pay for using DCB?</h2>
                                    <p class="lead-x"> With DCB, you can pay for various digital goods and services such as: </p>
                                    <div class="col-sm-6 text-left" style="padding-left: 0px;">
                                        <ul>
                                            <li class="lead-x">- Games</li>
                                            <li class="lead-x">- Comics</li>
                                            <li class="lead-x">- E-books</li>
                                            <li class="lead-x">- Wallpapers</li>
                                            <li class="lead-x">- Music and so on.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="switchable feature-large bg--grey">
                    <h2 class="text-center">How It Works</h2>
                    <p class="lead-x text-center spc"> Are you a digital content producer? Monapay was designed to empower<br> digital content producers like you. Simply integrate and start earning. </p>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="feature feature-5 boxed boxed--lg boxed--border"> <img src="img/icon-merchants.svg" width="60px" height="60px">
                                    <div class="feature__body">
                                        <h4>For Merchants</h4>
                                        <ul style="color:#505050;">
                                            <li>- Sign up as a merchant on <a class="link" href="www.monapay.com">monapay.com</a></li>
                                            <li>- Integrate monapay to existing products</li>
                                            <li>- Consumers check out with monapay</li>
                                            <li>- Track analytics and View your earnings on the dashboard</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="feature feature-5 boxed boxed--lg boxed--border"> <img src="img/user.svg" width="60px" height="60px">
                                    <div class="feature__body">
                                        <h4>For Users</h4>
                                        <ul style="color:#505050;">
                                            <li>- Access app or website for desired content</li>
                                            <li>- Buy tokens, subscribe or pay one-time fees</li>
                                            <li>- Check out with monapay</li>
                                            <li>- Pay from monapay wallet</li>
                                            <li>- Fund your wallet with airtime from your device.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <br>
                        </div>
                    </div>
                </section>

                <a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
                    <i class="stack-interface stack-up-open-big"></i>
                </a>

            </div>

            <?php include("footer.php"); ?>
