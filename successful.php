<!DOCTYPE html>
<html lang="en">
<head>
    <title>monapay</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <link rel="shortcut icon" href="img/" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="css/style-webapp.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet" type="text/css"> 
</head>
<body>
	<div class="container">
	  <div class="content-holder outer">
	  	<div class="content middle">
	  		<div class="logo text-center inner">
	            <div class="img-container">
		            <img class="" src="img/completed.png" alt="MonaPay">
		        </div>
		    </div>
		    <div class="text-content text-center">
		    	<h3 class="dark-text">Transaction Completed!</h3>
		    	<p class="dark-text small-font">You have purchased <span class="highlighted-text">500</span> Aboki run coins for <span class="highlighted-text">₦50.00</span></p>
		    	<p class="lighter-text small-font">New Balance</p>
		    	<h5 class="highlighted-text">₦417.35</h5>
				<a href="<?= $mainpath ?>welcome.php"><button class="button colored-button">finish</button></a>
		    </div>
	  	</div>
	  </div>
	</div>
</body>
</html> 