<?php
$mainpath = "/";
?>
    <!doctype html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <title>Monapay - Instant Airtime Payment Solution for Nigerians</title>
        <!-- Search Engine -->
        <meta name="description" content="Monapay enables you to monetize your products using airtime payments. Users are 10x more likely to use airtime to pay for digital goods. Sign up now.">
        <meta name="image" content="https://monapay.com/img/opengraph.png">
        <!-- Schema.org for Google -->
        <meta itemprop="name" content="Monapay - Instant Airtime Payment Solution for Nigerians">
        <meta itemprop="description" content="Monapay enables you to monetize your products using airtime payments. Users are 10x more likely to use airtime to pay for digital goods. Sign up now.">
        <meta itemprop="image" content="https://monapay.com/img/opengraph.png">
        <!-- Twitter -->
        <meta name="twitter:card" content="summary">
        <meta name="twitter:title" content="Monapay - Instant Airtime Payment Solution for Nigerians">
        <meta name="twitter:description" content="Monapay enables you to monetize your products using airtime payments. Users are 10x more likely to use airtime to pay for digital goods. Sign up now.">
        <!-- Open Graph general (Facebook, Pinterest & Google+) -->
        <meta name="og:title" content="Monapay - Instant Airtime Payment Solution for Nigerians">
        <meta name="og:description" content="Monapay enables you to monetize your products using airtime payments. Users are 10x more likely to use airtime to pay for digital goods. Sign up now.">
        <meta name="og:image" content="https://monapay.com/img/opengraph.png">
        <meta name="og:url" content="https://monapay.com">
        <meta name="og:site_name" content="Monapay Site">
        <meta name="og:type" content="website">


        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/stack-interface.css" rel="stylesheet" type="text/css" media="all">
        <link href="css/socicon.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/flickity.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet" type="text/css">
        <link href="css/font-roboto.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="css/devices.min.css" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="css/index-codepanel.css">
        <link rel="stylesheet" href="css/default-codepanel.css">
        <link rel="stylesheet" href="css/prism_light.css">
        <script defer src="js/default-codepanel.js"></script>
        <script defer src="js/index-codepanel.js"></script>
        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="img/Monapay_favicon.png">
        <link rel="icon" type="image/png" href="img/Monapay_favicon.png" sizes="32x32">
    </head>
    <style type="text/css">
        .hide-it {
            display: none !important;
        }

    </style>

    <body class="scroll-assist" data-smooth-scroll-offset="77">
        <a id="start"></a>
        <div class="nav-container">
            <div class="via-1491986892298" via="via-1491986892298" vio="monapay">
                <div class="bar bar--sm visible-xs">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-8 col-sm-2">
                                <a href="<?= $mainpath ?>home.php"> <img class="logo logo-dark" alt="logo" src="img/Asset%201monapay%20logo.svg"> <img class="logo logo-light" alt="logo" src="img/Asset%201monapay%20logo.svg"> </a>
                            </div>
                            <div class="col-xs-12 col-sm-5 text-right">
                                <a href="#" class="hamburger-toggle" data-toggle-class="#menu1;hidden-xs hidden-sm"> <i class="material-icons">menu</i> </a>
                            </div>
                        </div>
                    </div>
                </div>
                <nav id="menu1" class="bar bar--sm bar-1 hidden-xs" data-scroll-class="10vh:pos-fixed">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-2 col-sm-2 hidden-xs header1">
                                <div class="bar__module">
                                    <a href="<?= $mainpath ?>home.php"> <img class="logo logo-dark" alt="logo" src="img/Asset%201monapay%20logo.svg"> <img class="logo logo-light" alt="logo" src="img/Asset%201monapay%20logo.svg"> </a>
                                </div>
                            </div>
                            <div class="col-md-11 col-sm-12 text-right text-left-xs text-left-sm header2 mob-nav">
                                <div class="bar__module">
                                    <ul class="menu-horizontal text-left">
                                        <li> <a href="<?= $mainpath ?>home.php#how-it-works" class="inner-link" target="_self">
                                            How it Works
                                        </a> </li>
                                        <li> <a href="<?= $mainpath ?>home.php#features" class="inner-link" target="_self">
                                            Features
                                        </a> </li>
                                        <li> <a href="<?= $mainpath ?>home.php#developers" class="inner-link" target="_self">Developers</a>
                                        </li>
                                        <li> <a href="https://www.maliyo.com">
                                            Marketplace
                                        </a> </li>
                                    </ul>
                                </div>
                                <div class="bar__module">
                                    <div class="modal-instance sgn-up">
                                        <a class="btn btn--sm modal-trigger sign__up" href="#"> <span class="btn__text">
                            Demo Account
                        </span> </a>
                                        <div class="modal-container modal" data-modal-index="4">
                                            <div class="modal-content">
                                                <section class="unpad ">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-sm-6 col-md-offset-3 col-sm-offset-3 col-xs-12">
                                                                <div class="feature feature-1">
                                                                    <div class="feature__body boxed boxed--lg boxed--border text-center">
                                                                        <div class="modal-close modal-close-cross"></div>
                                                                        <!-- Demo Account form --->
                                                                        <div>
                                                                            <div class="text-block">
                                                                                <h3>Request for demo account</h3>
                                                                                <p>
                                                                                    As a developer, you can request for demo account and testing parameters.
                                                                                </p>
                                                                            </div>
                                                                            <?php include ("requestform.phtml"); ?>
                                                                            <!-- end of demo account form -->
                                                                            <!-- flip forms
                                                                                                <div id="login">
                                                                                                    <div class="text-block">
                                                                                                        <h3>Login to your account</h3>
                                                                                                        <p>
                                                                                                            Welcome back, sign in with your existing account credentials.
                                                                                                        </p>
                                                                                                    </div>
                                                                                                    <form>
                                                                                                        <div class="row">
                                                                                                            <div class="col-sm-12">
                                                                                                                <input type="email" name="Email Address" placeholder="Email Address">
                                                                                                            </div>
                                                                                                            <div class="col-sm-12">
                                                                                                                <input type="password" placeholder="Password">
                                                                                                            </div>
                                                                                                            <div class="col-sm-12">
                                                                                                                <button class="btn btn--primary type--uppercase" type="submit"><span class="btn__text">Login</span></button>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                    </form>
                                                                                                    <span class="type--fine-print block">Don't have an account yet? <a href="#" id="flip-login">Create account</a></span> <span class="type--fine-print block">Forgot your username or password? <a href="#" id="flip-recover">Recover account</a></span>
                                                                                                </div>

                                                                                                <div class="hidden" id="recover-account">
                                                                                                    <div class="row">
                                                                                                        <div class="text-block">
                                                                                                            <h3>Recover your account</h3>
                                                                                                            <p> Enter email address to send a recovery email. </p>
                                                                                                            <form>
                                                                                                                <div class="col-sm-12">
                                                                                                                    <input type="email" placeholder="Email Address"></div>
                                                                                                                <div class="col-sm-12">
                                                                                                                    <button class="btn btn--primary type--uppercase" type="submit"><span class="btn__text">Recover Account</span></button>
                                                                                                                </div>
                                                                                                            </form>
                                                                                                            <span class="type--fine-print block">Dont have an account yet? <a href="#" id="flip-signup-recover">Create account</a></span>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="hidden" id="signup">
                                                                                                    <div class="row">
                                                                                                        <div class="text-block">
                                                                                                            <h3>Sign up and start earning with monapay</h3>
                                                                                                            <p>Get started with monapay today, No credit card required.</p>
                                                                                                            <form>
                                                                                                                <div class="row">
                                                                                                                    <div class="col-sm-12"> <input type="email" name="Email Address" placeholder="Email Address"> </div>
                                                                                                                    <div class="col-sm-12"> <input type="password" name="Password" placeholder="Password"> </div>
                                                                                                                    <div class="col-sm-12"> <button type="submit" class="btn btn--primary type--uppercase"><span class="btn__text">Create Account</span></button> </div>
                                                                                                                    <div class="col-sm-12"> <span class="type--fine-print">By signing up, you agree to the <a href="terms-of-service.html">Terms of Service</a></span> </div>
                                                                                                                </div>
                                                                                                            </form>
                                                                                                            <span class="type--fine-print block">Already a user? <a href="#" id="flip-signup">Login</a></span>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>-->

                                                                        </div>
                                                                    </div>
                                                                    <!--end feature-->
                                                                </div>
                                                            </div>
                                                            <!--end of row-->
                                                        </div>
                                                        <!--end of container-->
                                                </section>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </nav>
                </div>
            </div>
            <div class="main">

                <div class="main-content">
                    <section class="cover custom--space--lg unpad--bottom switchable height-100">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="mt--2">
                                        <h1>Mobile payments. Simplified.</h1>
                                        <p class="lead">An Airtime payment solution for digital content providers in Nigeria.</p>
                                        <!-- demo button-->
                                        <div class="modal-instance">
                                            <a class="btn btn--purple  modal-trigger" href="#"> <span class="btn__text">Try Demo</span> </a>
                                            <div class="modal-container modal" data-modal-index="4">
                                                <div class="modal-content">
                                                    <section class="unpad ">
                                                        <div class="container">
                                                            <div class="row">
                                                                <div class="col-sm-6 col-md-offset-3 col-sm-offset-3 col-xs-12 iphone--modal">
                                                                    <div class="feature feature-1">
                                                                        <div class="feature__body boxed boxed--lg boxed--border text-center modal--container--hide">
                                                                            <!-- content goes here-->
                                                                            <div class="marvel-device iphone6 black">
                                                                                <div class="top-bar"></div>
                                                                                <div class="sleep"></div>
                                                                                <div class="volume"></div>
                                                                                <div class="camera"></div>
                                                                                <div class="sensor"></div>
                                                                                <div class="speaker"></div>
                                                                                <div class="screen">
                                                                                    <div class="modal-close modal-close-cross" style="top: .5em;"></div>
                                                                                    <!-- Content goes here -->
                                                                                    <iframe src="<?= $mainpath ?>index-webapp.php" width="100%" height="100%"></iframe>
                                                                                </div>
                                                                                <div class="home"></div>
                                                                                <div class="bottom-bar"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end feature-->
                                                                </div>
                                                            </div>
                                                            <!--end of row-->
                                                        </div>
                                                        <!--end of container-->
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- end of demo button-->
                                        <!-- sign up button for mobile phones -->
                                        <div class="modal-instance">
                                            <a class="btn modal-trigger sign__up mobile" href="#"> <span class="btn__text">Demo Account</span> </a>
                                            <div class="modal-container modal" data-modal-index="4">
                                                <div class="modal-content">
                                                    <section class="unpad ">
                                                        <div class="container">
                                                            <div class="row">
                                                                <div class="col-sm-6 col-md-offset-3 col-sm-offset-3 col-xs-12">
                                                                    <div class="feature feature-1">
                                                                        <!-- sign up form -->
                                                                        <div class="feature__body boxed boxed--lg boxed--border text-center">
                                                                            <div class="modal-close modal-close-cross"></div>
                                                                            <div class="text-block">
                                                                                <h3>Request for demo account</h3>
                                                                                <p>
                                                                                    As a developer, you can request for demo account and testing parameters.
                                                                                </p>
                                                                            </div>
                                                                            <?php include ("requestform.phtml"); ?>
                                                                        </div>
                                                                        <!-- end of sign in form -->
                                                                    </div>
                                                                    <!--end feature-->
                                                                </div>
                                                            </div>
                                                            <!--end of row-->
                                                        </div>
                                                        <!--end of container-->
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- sign up button for mobile phones -->
                                        <!-- watch video button -->
                                        <div class="modal-instance block play-vid">
                                            <div class="modal-trigger">
                                                <div class="video-play-icon video-play-icon--sm play--btn--blue" data-modal-index="0"></div>
                                                <span class="btn__text play-vid-text" style="cursor: pointer">Watch video</span>
                                            </div>
                                            <div class="modal-container">
                                                <div class="modal-content" data-width="50%" data-height="60%">
                                                    <div class="modal-close modal-close-cross"></div>
                                                    <iframe src="https://www.youtube.com/embed/0RngIjiIpbg?autoplay=1" allowfullscreen="allowfullscreen" data-src="https://www.youtube.com/embed/0RngIjiIpbg?autoplay=1"></iframe>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- watch video button -->
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <!--<div class="big-circle">
                            <div class="small-circle-1">
                            </div>
                            <div class="small-circle-2"></div>
                        </div>-->
                                    <img class="mona pos-bottom" alt="Image" src="img/Monapay-Hero.png" width="100%"></div>
                            </div>
                        </div>
                        <div class="pos-bottom pos-absolute col-xs-12">
                            <div class="container text-center">
                                <a href="#key-benefits" class="inner-link" id="scroll" target="_self">
                                    <span class="btn__text arror-downward"><i class="material-icons">arrow_downward</i></span></a>
                            </div>
                        </div>
                    </section>
                    <a id="key-benefits" class="in-page-link" data-scroll-id="feature"></a>
                    <section class="text-center imagebg" data-overlay="4" style="padding-top:0;">
                        <section class="text-center imagebg">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-7 col-md-5">
                                        <h2>Airtime for Payments</h2>
                                        <p class="lead">
                                            Users are 10x more likely to use airtime as a means of payment for digital goods.
                                        </p>
                                    </div>
                                </div>
                                <!--end of row-->
                            </div>
                            <!--end of container-->
                        </section>
                        <div class="background-image-holder"> <img alt="background" src="img/Monapay_Bg.jpg"> </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="feature feature-3 boxed boxed--lg boxed--border"> <img src="img/icon-convenience-white.svg" width="60px" height="60px">
                                        <h4>Convenience</h4>
                                        <p> Monetizing digital products has never been this easy. Monapay gives you access to customers who can pay for your products with their airtime. </p>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="feature feature-3 boxed boxed--lg boxed--border"> <img src="img/icon-payment-white.svg" width="60px" height="60px">
                                        <h4>Payment Types</h4>
                                        <p> Whether you wish to charge one-time fees, subscriptions or in-app billings, Monapay supports whatever works best for your business. </p>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="feature feature-3 boxed boxed--lg boxed--border"> <img src="img/icon-hassle-free-white.svg" width="60px" height="60px">
                                        <h4>Hassle-free</h4>
                                        <p> We have done all the hard work on licensing and partnerships. By simply integrating our API with your products, you start earning on the go. </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <a id="how-it-works" class="in-page-link" data-scroll-id="how-it-works"></a>
                    <section class="switchable switchable--switch">
                        <h2 class="text-center">How It Works</h2>
                        <p class="lead-x text-center spc"> Are you a digital content producer? Monapay was designed to empower<br> digital content producers like you. Simply integrate and start earning. </p>
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="video-cover border--round">
                                        <div class="background-image-holder">
                                            <img alt="image" src="img/Monapay-explainer-video.jpg" />
                                        </div>
                                        <div class="video-play-icon video-play-icon--sm">
                                        </div>
                                        <iframe src="https://www.youtube.com/embed/0RngIjiIpbg?autoplay=1" allowfullscreen="allowfullscreen" data-src="https://www.youtube.com/embed/0RngIjiIpbg?autoplay=1"></iframe>
                                    </div>
                                    <!--end video cover-->
                                </div>
                                <div class="col-sm-6 col-md-5">
                                    <ol class="process-3">
                                        <li class="process_item">
                                            <div class="process__number spces"><span>1</span></div>
                                            <div class="process__body">
                                                <h4 class="blue--text">Sign up</h4>
                                                <p> Create an account on our website, it’s quick and simple. </p>
                                            </div>
                                        </li>
                                        <li class="process_item">
                                            <div class="process__number"><span>2</span></div>
                                            <div class="process__body">
                                                <h4 class="blue--text">Add Products</h4>
                                                <p> Create as many product(s) as you want, edit your product and pricing information. </p>
                                            </div>
                                        </li>
                                        <li class="process_item">
                                            <div class="process__number"><span>3</span></div>
                                            <div class="process__body">
                                                <h4 class="blue--text">Integrate and Earn</h4>
                                                <p> Use our API documents to integrate and view your earnings from the merchant portal.</p>
                                            </div>
                                        </li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </section>
                    <a id="features" class="in-page-link" data-scroll-id="feature"></a>
                    <section class="switchable feature-large bg--grey">
                        <a id="dashboard" class="in-page-link" data-scroll-id="dashboard"></a>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-7"> <img alt="Image" src="img/Cross-Platform.png" style="width: 1000px; align-self: flex-end;"> </div>
                                <div class="col-sm-6 col-md-5">
                                    <div class="switchable__text dash">
                                        <h2 class="h3">Cross Platform</h2>
                                        <p class="lead-x"> With the merchant dashboard, shop admins can access transaction information, customer behaviour data and other cool performance metrics on any device. </p>
                                        <a href="#"> </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <a id="payment" class="in-page-link" data-scroll-id="payment"></a>
                    <section class="switchable feature-large switchable--switch">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-7 payment">
                                    <img alt="Image" class="image-holder" src="img/Pay_20now-game.png"> </div>
                                <div class="col-sm-6 col-md-5">
                                    <div class="switchable__text pay">
                                        <h2 class="h3">Direct Carrier Billing</h2>
                                        <p class="lead-x"> With our mobile-first commerce solution; you can easily integrate and start monetizing your digital products such as games, e-books, comics, animations, music and so much more. </p>
                                        <a href="#"> </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <a id="security" class="in-page-link" data-scroll-id="security"></a>
                    <section class="switchable feature-large bg--grey">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-7 security"> <img alt="Image" class="image-holder-security" src="img/Monapay-Secure.png"> </div>
                                <div class="col-sm-6 col-md-5">
                                    <div class="switchable__text sec">
                                        <h2 class="h3">Security</h2>
                                        <p class="lead-x"> Users are protected using a secure PIN system; this ensures that only the authorised account holder can make purchases.</p>
                                        <a href="#"> </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <a id="developers" class="in-page-link" data-scroll-id="developers"></a>
                    <section class="switchable feature-large bg--dark--blue">
                        <h2 class="text-center white-text">Easy integration across all platforms.</h2>
                        <p class="lead-x text-center white-text"> We created a Direct Carrier Billing payment API with a seamless integration process so you<br> can easily earn money from your ideas. If you require additional support, feel free to <a class="c-us" href="mailto:support@monapay.com?Subject=API%20Integration%20Request">contact us</a> </p>
                        <section class="text-center" style="padding-top: 0px; padding-bottom: 0px;">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="tabs-container" data-content-align="left">
                                            <ul class="tabs">
                                                <li class="active">
                                                    <div id="android-but" class="tab__title"> <span class="btn__text h6 android">Android</span> </div>
                                                </li>
                                                <li>
                                                    <div id="ios-but" class="tab__title"> <span class="btn__text h6 ios">iOS</span> </div>
                                                </li>
                                                <li>
                                                    <div id="web-but" class="tab__title"> <span class="btn__text h6 web">Web</span> </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <div id="notebook" class="container">
                            <nav>
                                <span id="api-method-selection"></span>
                            </nav>
                            <div id="editor">
                                <pre class="language-javascript"><span class="custom-line-numbers">1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
</span>

<code id="android" class="language-javascript"><span class="token keyword">Uri.</span>Builder builder<span class="token operator">=</span> <span class="token function">newUri.Builder</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

<span class="token keyword">builder<span class="token punctuation">.</span>scheme("http")</span> 
  .authority<span class="token punctuation">(</span><span class="token string">"beta.monapay.com<span class="token punctuation">:</span>80"</span><span class="token punctuation">)</span>
  .appendPath<span class="token punctuation">(</span><span class="token string">"v1"</span><span class="token punctuation">)</span> 
  .appendPath<span class="token punctuation">(</span><span class="token string">"merchant"</span><span class="token punctuation">)
   .appendPath<span class="token punctuation">(</span><span class="token string">"pay"</span><span class="token punctuation">)</span> 
  .appendQueryParameter<span class="token punctuation">(</span><span class="token string">"reference_id"</span><span class="token punctuation">,</span><span class="token string">"MG1489414003"</span><span class="token punctuation">)</span>
  .appendQueryParameter<span class="token punctuation">(</span><span class="token string">"merchant_id"</span><span class="token punctuation">,</span><span class="token string">"3"</span><span class="token punctuation">)</span>
  .appendQueryParameter<span class="token punctuation">(</span><span class="token string">"product_key"</span><span class="token punctuation">,</span><span class="token string">"25ab5370735e20391ce2df6329df5c1d9fa15c41"</span><span class="token punctuation">)</span>
  .appendQueryParameter<span class="token punctuation">(</span><span class="token string">"uuid"</span><span class="token punctuation">,</span><span class="token string">"456789876543456796gvbndcvbnmnbv"</span><span class="token punctuation">)</span>
  .appendQueryParameter<span class="token punctuation">(</span><span class="token string">"amount"</span><span class="token punctuation">,</span><span class="token string">"20000"</span><span class="token punctuation">)</span> 
   .appendQueryParameter<span class="token punctuation">(</span><span class="token string">"description"</span><span class="token punctuation">,</span><span class="token string">"Buy MaliyoToken5000 for 200 NGN"</span><span class="token punctuation">)</span>
   <span class="token keyword">String paymentUrl</span> Builder builder =  <span class="token operator">=</span> <span class="token function">builder.build<span class="token punctuation">(</span><span class="token punctuation">)</span>.toString</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
   WebView webView<span class="token operator">=</span><span class="token string">WebView</span> <span class="token function">findViewById</span><span class="token punctuation">(</span><span class="token string">'R<span class="token punctuation">.</span>id<span class="token punctuation">.</span>webView'</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
   
   <span class="token keyword">webView<span class="token punctuation">.</span>loadUrl<span class="token punctuation">(</span><span class="token string">paymentUrl</span><span class="token punctuation">)</span></span> 
    </code>
    
<code id="ios" class="language-javascript hide-it"><span class="token keyword">NSURLComponents</span><span class="token punctuation">*</span>components<span class="token punctuation">[</span><span class="token string">NSURLComponents componentsWithString<span class="token punctuation">:</span>@"http://beta.monapay.com:80/v1/merchant/pay"</span><span class="token punctuation">]</span><span class="token punctuation">;</span>

<span class="token keyword">NSURLQueryItem</span><span class="token punctuation">*</span>reference_id<span class="token punctuation">[</span><span class="token string">NSURLQueryItem queryItemWithName<span class="token punctuation">:</span>@"merchant_id"</span><span class="token string">value<span class="token punctuation">:</span>@"3"</span><span class="token punctuation">]</span><span class="token punctuation">;</span>

<span class="token keyword">NSURLQueryItem</span><span class="token punctuation">*</span>product_key<span class="token punctuation">[</span><span class="token string">NSURLQueryItem queryItemWithName<span class="token punctuation">:</span>@"product_key"</span><span class="token string">value<span class="token punctuation">:</span>@"25ab5370735e20391ce2df6329df5c1d9fa15c41"</span><span class="token punctuation">]</span><span class="token punctuation">;</span>

<span class="token keyword">NSURLQueryItem</span><span class="token punctuation">*</span>uuid<span class="token punctuation">[</span><span class="token string">NSURLQueryItem queryItemWithName<span class="token punctuation">:</span>@"uuid"</span><span class="token string">value<span class="token punctuation">:</span>@"456789876543456796gvbndcvbnmnbv"</span><span class="token punctuation">]</span><span class="token punctuation">;</span>

<span class="token keyword">NSURLQueryItem</span><span class="token punctuation">*</span>amount<span class="token punctuation">[</span><span class="token string">NSURLQueryItem queryItemWithName<span class="token punctuation">:</span>@"amount"</span><span class="token string">value<span class="token punctuation">:</span>@"20000"</span><span class="token punctuation">]</span><span class="token punctuation">;</span>

<span class="token keyword">NSURLQueryItem</span><span class="token punctuation">*</span>description<span class="token punctuation">[</span><span class="token string">NSURLQueryItem queryItemWithName<span class="token punctuation">:</span>@"description"</span><span class="token string">value<span class="token punctuation">:</span>@"Buy MaliyoToken5000 for 200 NGN"</span><span class="token punctuation">]</span><span class="token punctuation">;</span>

<span class="token keyword">components</span><span class="token punctuation">.</span>queryItems =<span class="token punctuation">@</span><span class="token punctuation">[</span>"reference_id, merchant_id, product_key, uuid, amount, description"</span><span class="token punctuation">]</span><span class="token punctuation">;</span>

<span class="token keyword">NSURL</span><span class="token punctuation">*</span>paymentUrl = components<span class="token punctuation">.</span>URL<span class="token punctuation">;</span>

<span class="token punctuation">[</span>"self<span class="token punctuation">.</span>webView loadRequest<span class="token punctuation">:</span><span class="token punctuation">[</span>NSURLRequest requestWithURL<span class="token punctuation">:</span>paymentUrl"</span>><span class="token punctuation">]</span><span class="token punctuation">]</span><span class="token punctuation">;</span>
   </code>
   
<code id="web" class="language-javascript hide-it"><span class="token keyword">const</span>
<span class="token keyword">builder<span class="token punctuation">.</span>scheme("http")</span> 
  $referenceId<span class="token punctuation">=</span><span class="token string">MG<span class="token punctuation">.</span>time</span><span class="token punctuation">(</span><span class="token punctuation">)</span>
  $productKey <span class="token punctuation"> = </span><span class="token string"> "25ab5370735e20391ce2df6329df5c1d9fa15c41"</span><span class="token punctuation">;</span>
  $deviceUuid <span class="token punctuation"> = </span><span class="token string"> "456789876543456796gvbndcvbnmnbv"</span><span class="token punctuation">;</span>
  $amount <span class="token punctuation"> = </span><span class="token string"> "20000"</span><span class="token punctuation">;</span>
  $description <span class="token punctuation"> = </span><span class="token string"> "Buy MaliyoToken5000 for 200 NGN"</span><span class="token punctuation">;</span>
$data<span class="token punctuation"> = </span>array<span class="token punctuation">(</span>
  "reference_id"<span class="token punctuation"> => </span>$referenceId<span class="token punctuation">,</span>
  "merchant_id"<span class="token punctuation"> => </span>$merchantId<span class="token punctuation">,</span>
  "product_key"<span class="token punctuation"> => </span>$productKey<span class="token punctuation">,</span>
  "uuid"<span class="token punctuation"> => </span>$deviceUuid<span class="token punctuation">,</span>
  "amount"<span class="token punctuation"> => </span>$amount<span class="token punctuation">,</span>
  "description"<span class="token punctuation"> => </span>$description
  <span class="token punctuation">)</span><span class="token punctuation">;</span>
$paymentUrl<span class="token punctuation"> = </span><span class="token string">"http://beta.monapay.com:80/v1/merchant/pay?"<span class="token punctuation"> . http_build_query</span></span><span class="token punctuation">(</span>$data<span class="token punctuation">)</span><span class="token punctuation">;</span>
header<span class="token punctuation">(</span>"Location: $paymentUrl"<span class="token punctuation">)</span><span class="token punctuation">;</span>
</code>
       </pre>
                            </div>
                            <br>
                            <center>
                                <a class="btn btn--white--inverse btn--sm text-center" href="<?= $mainpath ?>api-doc.php">
                                    <span class="btn__text">
                                        API Documentation
                                    </span>
                                </a>
                            </center>
                        </div>
                    </section>
                    <!--<section class="bg--secondary">
                <div class="container">
                    <div class="row">
                        <div class="slider slider--inline-arrows slider--arrows-hover text-center">
                            <ul class="slides">
                                <li class="col-sm-3 col-xs-6"> <img alt="Image" class="image--xxs" src="img/partner-1.png"> </li>
                                <li class="col-sm-3 col-xs-6"> <img alt="Image" class="image--xxs" src="img/partner-2.png"> </li>
                                <li class="col-sm-3 col-xs-6"> <img alt="Image" class="image--xxs" src="img/partner-3.png"> </li>
                                <li class="col-sm-3 col-xs-6"> <img alt="Image" class="image--xxs" src="img/partner-4.png"> </li>
                                <li class="col-sm-3 col-xs-6"> <img alt="Image" class="image--xxs" src="img/partner-5.png"> </li>
                                <li class="col-sm-3 col-xs-6"> <img alt="Image" class="image--xxs" src="img/partner-6.png"> </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>-->
                    <!--<div class="loader"></div>-->
                    <a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
                        <i class="stack-interface stack-up-open-big"></i>
                    </a>

                </div>


                <?php include("footer.php"); ?>
