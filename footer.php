<div class="footer_spacer"></div>
<section class="imagebg switchable text-center height-auto" data-overlay="5">
    <div class="background-image-holder"><img alt="background" src="img/get-started.jpg"></div>
    <div class="container pos-vertical-center">
        <div class="row">
            <div class="col-sm-7 col-md-5">
                <h2>Request a FREE Demo Account</h2>
                <p class="lead"> Get inspired, create your products and let’s help you earn money conveniently wherever you are!</p>
                <div class="row">
                    <form method="post" class="form-email" data-success="Thanks for your request, we'll send demo account details shortly." data-error="Please fill in all fields correctly.">

                        <div class="col-sm-12 footer-form hidden">
                            <input type="text" name="hidden-none" placeholder="Your Name" class="validate-required validate-email">
                        </div>

                        <div class="col-sm-12 footer-form">
                            <input type="text" name="Name" placeholder="Your Name" class="validate-required validate-email">
                        </div>

                        <div class="col-sm-12 footer-form">
                            <input type="email" name="Email" placeholder="Email Address" class="validate-required validate-email">
                        </div>
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn--white--inverse btn--sm"><span class="btn__text">Create Free Account</span></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- for mobile devices -->
    <footer class="text-center-xs space--xs mobile">
        <div class="container">
            <div class="row">
                <div class="col-sm-2 footer2">
                    <a href="<?= $mainpath ?>home.php">
                        <div class="vnu"><img alt="Image" class="logo" src="img/Monapay%20Logomark_Light.svg"></div>
                    </a>
                </div>
                <div class="col-sm-5 footer1">
                    <p class="f-1"> <a href="<?= $mainpath ?>about-us.php"><span class="h6">About</span></a>
                        <a href="<?= $mainpath ?>terms-of-service.php"><span class="h6">Terms of Service</span></a>
                        <a href="<?= $mainpath ?>api-doc.php"><span class="h6">Developer</span></a></p>
                </div>
                <div class="col-sm-5 footer3">
                    <p class="f-2"><a href="<?= $mainpath ?>privacy-policy.php"><span class="h6">Privacy Policy</span></a>
                        <a href="http://www.maliyo.com"><span class="h6">Marketplace</span></a>
                        <a href="<?= $mainpath ?>help-support.php"><span class="h6">Help & Support</span></a> </p>
                </div>
            </div>
        </div>
    </footer>
    <!-- end of mobile device footer -->
</section>
<footer class="text-center-xs space--xs">
    <div class="container">
        <div class="row">
            <div class="col-sm-5 footer1">
                <ul class="list-inline menu-horizontal">
                    &nbsp; &nbsp;
                    <li> <a href="<?= $mainpath ?>about-us.php"><span class="h6">About</span></a> </li> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    <!--<li> <a href="faq.html"><span class="h6">FAQ</span></a> </li> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;-->
                    <!--<li> <a href="#"><span class="h6">Feedback</span></a> </li> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;-->
                    <li> <a href="<?= $mainpath ?>terms-of-service.php"><span class="h6">Terms of Service</span></a> </li> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    <li><a href="<?= $mainpath ?>api-doc.php"><span class="h6">Developer</span></a></li>
                </ul>
            </div>
            <div class="col-sm-2 footer2">
                <a href="<?= $mainpath ?>home.php">
                    <div class="vnu"><img alt="Image" class="logo" src="img/Monapay%20Logomark_Light.svg"></div>
                </a>
            </div>
            <div class="col-sm-5 footer3">
                <center>
                    <ul class="list-inline menu-horizontal">
                        &nbsp; &nbsp;
                        <li><a href="<?= $mainpath ?>privacy-policy.php"><span class="h6">Privacy Policy</span></a></li> &nbsp; &nbsp; &nbsp; &nbsp;
                        <li><a href="http://www.maliyo.com"><span class="h6">Marketplace</span></a></li>&nbsp; &nbsp; &nbsp; &nbsp;
                        <li> <a href="<?= $mainpath ?>help-support.php"><span class="h6">Help & Support</span></a> </li>
                    </ul>
                </center>
            </div>
        </div>
    </div>
</footer>

</div>

<script src="js/jquery-3.1.1.min.js"></script>
<script src="js/parallax.js"></script>
<script src="js/smooth-scroll.min.js"></script>
<script src="js/scripts.js"></script>

<script type="text/javascript">
    $('#android-but').click(function() {
        $('#android-but').addClass('selected');
        $('#ios-but').removeClass('selected');
        $('#web-but').removeClass('selected');

        $('#android').removeClass('hide-it');
        $('#ios').addClass('hide-it');
        $('#web').addClass('hide-it');

    });

    $('#ios-but').click(function() {
        $('#ios-but').addClass('selected');
        $('#web-but').removeClass('selected');
        $('#web-but').removeClass('selected');

        $('#android').addClass('hide-it');
        $('#ios').removeClass('hide-it');
        $('#web').addClass('hide-it');

    });

    $('#web-but').click(function() {
        $('#web-but').addClass('selected');
        $('#ios-but').removeClass('selected');
        $('#android-but').removeClass('selected');

        $('#android').addClass('hide-it');
        $('#ios').addClass('hide-it');
        $('#web').removeClass('hide-it');

    });


    function swapMobileTab() {
        //get values of the input
        swap_val = $('.mobileTab').val();

        if (swap_val == 1) {

            $('#android').removeClass('hide-it');
            $('#ios').addClass('hide-it');
            $('#web').addClass('hide-it');

        } else if (swap_val == 2) {

            $('#android').addClass('hide-it');
            $('#ios').removeClass('hide-it');
            $('#web').addClass('hide-it');

        } else {

            $('#android').addClass('hide-it');
            $('#ios').addClass('hide-it');
            $('#web').removeClass('hide-it');
        }
    }

    // Controls flip login / signup / recover-account  
    $('#flip-login').click(function() {
        $('#signup').removeClass('hidden');
        $('#login').addClass('hidden');
        $('#recover-account').addClass('hidden');
    });
    $('#flip-signup').click(function() {
        $('#login').removeClass('hidden');
        $('#signup').addClass('hidden');
        $('#recover-account').addClass('hidden');
    });
    $('#flip-recover').click(function() {
        $('#recover-account').removeClass('hidden');
        $('#signup').addClass('hidden');
        $('#login').addClass('hidden');
    });

    $('#flip-signup-recover').click(function() {
        $('#recover-account').addClass('hidden');
        $('#signup').removeClass('hidden');
        $('#login').addClass('hidden');
    });

</script>

<script>
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-99608137-1', 'auto');
    ga('send', 'pageview');

</script>
</body>

</html>
