<!DOCTYPE html>
<html lang="en">
<head>
    <title>monapay</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <link rel="shortcut icon" href="img/" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="css/style-webapp.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet" type="text/css"> <script type="text/javascript" src="js/jquery-3.2.1.js"></script>
	<script type="text/javascript" src="js/pincode-input.js"></script>
    <script>
        $(document).ready(function() {
            $('#pincode-input1').pincodeInput({hidedigits:false,complete:function(value, e, errorElement){           	
            	$("#pincode-callback").html("This is the 'complete' callback firing. Current value: " + value);
            }});
            $('#pincode-input4').pincodeInput({hidedigits:false,inputs:4});
            $('#pincode-input3').pincodeInput({hidedigits:false,inputs:5});
            $('#pincode-input2').pincodeInput({hidedigits:false,inputs:6,complete:function(value, e, errorElement){
            	$("#pincode-callback").html("Complete callback from 6-digit test: Current value: " + value);
            }});           
        });
    </script>
    
</head>
<body>
	<div class="container">
	  <div class="content-holder outer">
	  	<div class="content middle">
	  		<div class="logo text-center inner">
		        <div class="logo-container bottom-space">
		            <img class="" src="img/Logo.png" alt="MonaPay">
		        </div>
		    </div>
		    <div class="text-header text-center">
		    	<h4 class="dark-text">Aboki Runner</h4>
		    	<p class="dark-text">Buy <span class="highlighted-text">500</span> Aboki Run coins for <span class="highlighte-text">₦50.00</span></p>
		    </div>
		    <div class="">
		    	<form>
		    	<center>
		    		<h4><span class="dark-text">+234814241XXXX</span></h4>
		    		</center>
		    		<p class="dark-text text-center small-font">Enter your pin</p>
					<div class="pincode">
					<input type="password"  name="pin" id="pincode-input1">
					</div>
		    		<br>
		    	</form>
		    	
				<a href="<?= $mainpath ?>fund_with_monapay.php"><button class="button colored-button">Continue</button></a>
                
		    </div>
	  	</div>
	  </div>
	</div>
</body>
</html> 