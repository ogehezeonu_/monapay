<!DOCTYPE html>
<html lang="en">
<head>
    <title>monapay</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <link rel="shortcut icon" href="img/" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="css/style-webapp.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet" type="text/css"> 
</head>
<body>
	<div class="container">
	  <div class="content-holder outer">
	  	<div class="content middle">
	    <div class="logo text-center inner">
		       <div class="logo-container">
		            <img class="" src="img/Logo.png" alt="MonaPay">
		        </div>
		    </div>
		    <div class="text-content text-center">
		    <p class="dark-text small-font">Product description:</p>
		    	<div class="product-description"><span class="thumb-sm avatar m-b-n-sm">
                    <img src="img/Bitmap.png" alt="..."></span>
				</div>
		    	<p class="dark-text big-font">Buy <span class="highlighted-text">500</span> Aboki Run coins for <span class="highlighted-text">₦50</span></p>
		    	<center>
		    		<h4><span class="dark-text">+234814241XXXX</span></h4>
		    		</center>
		    	<p class="lighter-text small-font">Current Balance</p>
		    	<h5 class="highlighted-text">₦467.35</h5>
				<a href="<?= $mainpath ?>successful.php"><button class="button colored-button">pay now</button></a>
	    		<a class="lighter-text" onclick="javascript:window.close()" style="cursor: pointer">Cancel</a>
		    </div>
	  	</div>
	  </div>
	</div>
</body>
</html> 