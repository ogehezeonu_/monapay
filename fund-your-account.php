<!DOCTYPE html>
<html lang="en">
<head>
    <title>monapay</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <link rel="shortcut icon" href="img/" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="css/style-webapp.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet" type="text/css">  
</head>
<body>
	<div class="container">
	  <div class="content-holder outer" id="fund-account">
	  	<div class="content middle">
	    <div class="logo text-center inner">
		       <div class="logo-container">
		            <img class="" src="img/Logo.png" alt="MonaPay">
		        </div>
		    </div>
		    <div class="text-content">
		    	<center>
		    		<h4><span class="dark-text">+234814241XXXX</span></h4>
		    		</center>
		    	<p class="dark-text text-center">Fund your account with Monapay</p>
		    	<form class="">
					<input id="amount" type="radio" name="amount" value="50"> &nbsp; &nbsp;<span class="dark-text small-font">₦50</span> <br>
					<input id="amount" type="radio" name="amount" value="100"> &nbsp; &nbsp;<span class="dark-text small-font">₦100</span> <br>
					<input id="amount" type="radio" name="amount" value="200"> &nbsp; &nbsp;<span class="dark-text small-font">₦200</span> <span class="highlighted-text">(Recommended)</span> <br>
					<input id="amount" type="radio" name="amount" value="300"> &nbsp; &nbsp;<span class="dark-text small-font">₦300</span> <br>
					<input id="amount" type="radio" name="amount" value="400"> &nbsp; &nbsp;<span class="dark-text small-font">₦400</span> <br>
					<input id="amount" type="radio" name="amount" value="500"> &nbsp; &nbsp;<span class="dark-text small-font">₦500</span> <br>

<br>
		    	</form>

		    	<button class="button colored-button" id="proceed">Proceed</button>
		   
		
    			<center>
	    			<a class="lighter-text" href="<?= $mainpath ?>index-webapp.php" onclick="history.back(-1)" style="cursor: pointer">Back</a>
	    		</center>
		    </div>



		  
	  	</div>
	  </div>


	  <div class="content-holder outer" id="funded" style="display: none">
	  	<div class="content middle">
	  		<div class="logo text-center inner">
		       
	            <div class="img-container">
		            <img class="" src="img/completed.png" alt="MonaPay">
		        </div>
		    </div>
		    <div class="text-content text-center">
		    	<h3 class="dark-text">Transaction Successful!</h3>
		    	<p class="dark-text small-font">You have successfully funded your account.</p>
		    	<p class="lighter-text small-font">New Balance</p>
		    	<h5 class="highlighted-text">₦<span id="funded-total"></span></h5>
				<a href="<?= $mainpath ?>welcome.php"><button class="button colored-button" onclick="javascript:window.close()">finish</button></a>
		    </div>
	  	</div>
	  </div>


	</div>
</body>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript">

$("#proceed").click(function(){
	var amount = document.querySelector('input[name="amount"]:checked').value;

	total = parseFloat(amount) + parseFloat(15.35);

	$("#fund-account").hide();
	$("#funded").show();
	$("#funded-total").html(total);
})
	
</script>
</html> 