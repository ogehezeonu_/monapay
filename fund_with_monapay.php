<!DOCTYPE html>
<html lang="en">
<head>
    <title>monapay</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <link rel="shortcut icon" href="img/" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="css/style-webapp.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet" type="text/css"> 
</head>
<body>
	<div class="container">
	  <div class="content-holder outer">
	  	<div class="content middle">
	  		<div class="logo text-center inner">
		       <div class="logo-container">
		            <img src="img/Logo.png" alt="MonaPay">
		        </div>
		    </div>
		    <div class="text-content text-center">
		    <p class="dark-text">Buy <span class="highlighted-text">500</span> Aboki Run coins for <span class="highlighte-text">₦50.00</span></p>
		    	<center>
		    		<h4><span class="dark-text">+234814241XXXX</span></h4>
		    		</center>
		    	<p class="lighter-text small-font">Current Balance</p>
                <h5 class="red">₦15.35</h5>
		    	<p class="dark-text small-font">You have insufficient balance for this transaction.</p>
				<a href="<?= $mainpath ?>fund-your-account.php"><button class="button colored-button">fund with monapay</button></a>
		    </div>
	  	</div>
	  </div>
	</div>
</body>
</html> 