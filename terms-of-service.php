<?php
$mainpath = "/";
?>
    <!doctype html>
    <html lang="en">

    <head>
        <!-- COMMON TAGS -->
        <meta charset="utf-8">
        <title>Terms and Conditions</title>
        <!-- Search Engine -->
        <meta name="description" content="Terms and Conditions to guide end-users and merchants and developers using Monapay.  ">
        <!-- Schema.org for Google -->
        <meta itemprop="name" content="Terms and Conditions">
        <meta itemprop="description" content="Terms and Conditions to guide end-users and merchants and developers using Monapay.  ">
        <!-- Twitter -->
        <meta name="twitter:card" content="summary">
        <meta name="twitter:title" content="Terms and Conditions">
        <meta name="twitter:description" content="Terms and Conditions to guide end-users and merchants and developers using Monapay.  ">
        <!-- Open Graph general (Facebook, Pinterest & Google+) -->
        <meta name="og:title" content="Terms and Conditions">
        <meta name="og:description" content="Terms and Conditions to guide end-users and merchants and developers using Monapay.  ">
        <meta name="og:image" content="https://monapay.com/img/opengraph.png">
        <meta name="og:url" content="https://monapay.com">
        <meta name="og:site_name" content="Monapay Site">
        <meta name="og:type" content="website">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/stack-interface.css" rel="stylesheet" type="text/css" media="all">
        <link href="css/socicon.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/flickity.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet" type="text/css">
        <link href="css/font-roboto.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="css/devices.min.css" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="css/index-codepanel.css">
        <link rel="stylesheet" href="css/default-codepanel.css">
        <link rel="stylesheet" href="css/prism_light.css">
        <script defer src="js/default-codepanel.js"></script>
        <script defer src="js/index-codepanel.js"></script>
        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="img/Monapay_favicon.png">
        <link rel="icon" type="image/png" href="img/Monapay_favicon.png" sizes="32x32">
    </head>
    <style type="text/css">
        .hide-it {
            display: none !important;
        }
    </style>

<body class="scroll-assist" data-smooth-scroll-offset="77">
<a id="start"></a>
<div class="nav-container">
    <div class="via-1491986892298" via="via-1491986892298" vio="monapay">
        <div class="bar bar--sm visible-xs">
            <div class="container">
                <div class="row">
                    <div class="col-xs-8 col-sm-2">
                        <a href="<?= $mainpath ?>home.php"> <img class="logo logo-dark" alt="logo" src="img/Asset%201monapay%20logo.svg"> <img class="logo logo-light" alt="logo" src="img/Asset%201monapay%20logo.svg"> </a>
                    </div>
                    <div class="col-xs-12 col-sm-5 text-right">
                        <a href="#" class="hamburger-toggle" data-toggle-class="#menu1;hidden-xs hidden-sm"> <i class="material-icons">menu</i> </a>
                    </div>
                </div>
            </div>
        </div>
        <nav id="menu1" class="bar bar--sm bar-1 hidden-xs" data-scroll-class="10vh:pos-fixed">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-sm-2 hidden-xs header1">
                        <div class="bar__module">
                            <a href="<?= $mainpath ?>home.php"> <img class="logo logo-dark" alt="logo" src="img/Asset%201monapay%20logo.svg"> <img class="logo logo-light" alt="logo" src="img/Asset%201monapay%20logo.svg"> </a>
                        </div>
                    </div>
                    <div class="col-md-11 col-sm-12 text-right text-left-xs text-left-sm header2">
                        <div class="bar__module">
                            <ul class="menu-horizontal text-left">
                                <li> <a href="<?= $mainpath ?>home.php#how-it-works" class="inner-link" target="_self">
                                        How it Works
                                    </a> </li>
                                <li> <a href="<?= $mainpath ?>home.php#features" class="inner-link" target="_self">
                                        Features
                                    </a> </li>
                                <li> <a href="<?= $mainpath ?>home.php#developers" class="inner-link" target="_self">Developers</a>
                                </li>
                                <li> <a href="http://www.maliyo.com">
                                        Marketplace
                                    </a> </li>
                            </ul>
                        </div>
                        <div class="bar__module">
                            <div class="modal-instance sgn-up">
                                <a class="btn btn--sm modal-trigger sign__up" href="#"> <span class="btn__text">
                            Demo Account
                        </span> </a>
                                <div class="modal-container modal" data-modal-index="4">
                                    <div class="modal-content">
                                        <section class="unpad ">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-sm-6 col-md-offset-3 col-sm-offset-3 col-xs-12">
                                                        <div class="feature feature-1">
                                                            <div class="feature__body boxed boxed--lg boxed--border text-center">
                                                                <div class="modal-close modal-close-cross"></div>
                                                                <!-- Demo Account form --->
                                                                <div>
                                                                    <div class="text-block">
                                                                        <h3>Request for demo account</h3>
                                                                        <p>
                                                                            As a developer, you can request for demo account and testing parameters.
                                                                        </p>
                                                                    </div>
                                                                    <?php include ("requestform.phtml"); ?>
                                                                    <!-- end of demo account form -->
                                                                    <!-- flip forms
                                                                                            <div id="login">
                                                                                                <div class="text-block">
                                                                                                    <h3>Login to your account</h3>
                                                                                                    <p>
                                                                                                        Welcome back, sign in with your existing account credentials.
                                                                                                    </p>
                                                                                                </div>
                                                                                                <form>
                                                                                                    <div class="row">
                                                                                                        <div class="col-sm-12">
                                                                                                            <input type="email" name="Email Address" placeholder="Email Address">
                                                                                                        </div>
                                                                                                        <div class="col-sm-12">
                                                                                                            <input type="password" placeholder="Password">
                                                                                                        </div>
                                                                                                        <div class="col-sm-12">
                                                                                                            <button class="btn btn--primary type--uppercase" type="submit"><span class="btn__text">Login</span></button>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                </form>
                                                                                                <span class="type--fine-print block">Don't have an account yet? <a href="#" id="flip-login">Create account</a></span> <span class="type--fine-print block">Forgot your username or password? <a href="#" id="flip-recover">Recover account</a></span>
                                                                                            </div>

                                                                                            <div class="hidden" id="recover-account">
                                                                                                <div class="row">
                                                                                                    <div class="text-block">
                                                                                                        <h3>Recover your account</h3>
                                                                                                        <p> Enter email address to send a recovery email. </p>
                                                                                                        <form>
                                                                                                            <div class="col-sm-12">
                                                                                                                <input type="email" placeholder="Email Address"></div>
                                                                                                            <div class="col-sm-12">
                                                                                                                <button class="btn btn--primary type--uppercase" type="submit"><span class="btn__text">Recover Account</span></button>
                                                                                                            </div>
                                                                                                        </form>
                                                                                                        <span class="type--fine-print block">Dont have an account yet? <a href="#" id="flip-signup-recover">Create account</a></span>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="hidden" id="signup">
                                                                                                <div class="row">
                                                                                                    <div class="text-block">
                                                                                                        <h3>Sign up and start earning with monapay</h3>
                                                                                                        <p>Get started with monapay today, No credit card required.</p>
                                                                                                        <form>
                                                                                                            <div class="row">
                                                                                                                <div class="col-sm-12"> <input type="email" name="Email Address" placeholder="Email Address"> </div>
                                                                                                                <div class="col-sm-12"> <input type="password" name="Password" placeholder="Password"> </div>
                                                                                                                <div class="col-sm-12"> <button type="submit" class="btn btn--primary type--uppercase"><span class="btn__text">Create Account</span></button> </div>
                                                                                                                <div class="col-sm-12"> <span class="type--fine-print">By signing up, you agree to the <a href="terms-of-service.html">Terms of Service</a></span> </div>
                                                                                                            </div>
                                                                                                        </form>
                                                                                                        <span class="type--fine-print block">Already a user? <a href="#" id="flip-signup">Login</a></span>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>-->

                                                                </div>
                                                            </div>
                                                            <!--end feature-->
                                                        </div>
                                                    </div>
                                                    <!--end of row-->
                                                </div>
                                                <!--end of container-->
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</div>
<div class="main-container">
        <div class="main-content">
            <section class="text-center imagebg space--md" data-overlay="5">
                <div class="background-image-holder"><img alt="background" src="img/terms-of-service-purple.jpg"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-10 col-md-8">
                            <h1 class="white-text">Terms of Service</h1>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="container">
                    <div class="row flex">
                        <div class="col-sm-6">
                            <nav class="nav-sidebar">
                                <ul class="nav">

                                    <li><a href="<?= $mainpath ?>terms-of-service.php" class="active" target="_self">Terms of Service</a><br></li>
                                    <li><a href="<?= $mainpath ?>privacy-policy.php" target="_self">Privacy policy</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-sm-10">
                            <div class="container">
                                <div class="row">
                                    <!-- content for page goes here -->
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-sm-10 col-md-10 text-left tp">
                                                <p> Please read these Terms of Service ("Terms", "Terms of Service") carefully before using this website and any of monapay services (“Services”) operated by monapay (“us”, “we” or “our”). Your access to and use of the Services is conditioned on your acceptance of and compliance with these Terms. These Terms apply to all visitors, users, merchants and others who access or use the Services. You acknowledge that by checking the “Agree” box when you open a monapay account, you agree to abide by the following Terms of Service as amended from time to time (“Terms of Service”) concerning your use of the digital content and electronic money or airtime value-added service provided by monapay. If there is any part of the Terms of Service you need clarification on, please contact us at <a href="https://www.monapay.com/support.">https://www.monapay.com/support.</a><br>By accessing or using the Services, you agree to be bound by these Terms. </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-10 col-md-10 text-left">
                                        <h5><b>Definition and Interpretations</b></h5>
                                        <p>
                                            <p>In this Terms of Service, the following words have specific meanings set for them:</p>
                                        </p>
                                        <p>“<b>Account</b>” means your monapay user or merchant account which holds airtime or electronic money and which your phone number(s) is linked to;</p>
                                        <p>“<b>Account Information</b>” means any or/and all of the following: your phone number, your email, your username, your PIN (for users), your password (for merchants), information you use to log in to your account, security questions and answers and other account credentials and information specific to your account.</p>
                                        <p>“<b>Deposit</b>” means the crediting of funds to your monapay account through airtime.</p>
                                        <p>“<b>Fees</b>” means any and all fees and charges levied by us for your use of the monapay services, which may be amended by us from time to time in accordance with these Terms of Service.</p>
                                        <p>“<b>In-App Purchase</b>” is a service whereby a user buys digital goods for the enhancement or improvement of the consumption of content from a merchant through monapay.</p>
                                        <p>“<b>Merchant</b>” means any individual, business or commercial entity that accepts payments through Direct Carrier Billing; is appropriately registered with monapay to accept transactions from your monapay account.</p>
                                        <p>“<b>Payment</b>” means any payment made using your airtime through monapay; the debiting of an amount of electronic money from your monapay account and the subsequent crediting of such amount to a Merchant account, as designated by you (including subscription billing).</p>
                                        <p>“<b>PIN</b>” means the personal identification number which you have set to be used with your account.</p>
                                        <p>“<b>Subscription Billing</b>” is a service whereby a user requests that regular payments be made from their account at specified intervals to a merchant.</p>
                                        <p>“<b>Support</b>” can be contacted through <a href="https://www.monapay.com/support.">https://www.monapay.com/support.</a></p>
                                        <p>“<b>Transaction</b>” means a payment; a withdrawal; a deposit and in each case, less any applicable fees.</p>
                                        <p>“<b>User</b>” means any person who meets all membership and eligibility requirements set out in this Terms of Service.</p>
                                        <p>“<b>User site</b>” means the application accessed by users using their account information, where users can view their transaction history, deposit funds into their account and purchase digital content or products.</p>
                                        <p>“<b>Website</b>” means <a href="www.monapay.com">www.monapay.com</a> or such other website through which we may offer the monapay Services from time to time.</p>
                                        <p>“<b>Withdrawal</b>” means removing funds from your monapay account through a pre-registered bank account (applies to merchants only).</p>
                                        <p>“<b>You</b>” or “<b>Your</b>” means the person to whom this Terms of Service shall apply.</p>

                                        <p>This Terms of Service shall apply to all users and merchants.</p>
                                        <p>Section headings shall not affect the interpretation of this Terms of Service.</p>
                                        <p>A person includes a natural person, corporate or unincorporated body.</p>
                                        <p>Unless the context otherwise requires, words in the singular shall include the plural and vice versa.</p>

                                    </div>

                                    <div class="col-sm-10 col-md-10 text-left">
                                        <h5><b>General</b></h5>
                                        <p> It is mandatory to agree to the Terms of Service as it forms a legally binding contract between you and monapay once you register to become a user or merchant. </p>
                                        <p> We may change the Terms of Service from time to time and we will notify you of changes through your preferred channel of communication registered with your monapay account; and by posting notice of the changes on our website with a link to the amended Terms of Service. We will provide at least one (1) month notice before the proposed changes come into effect or else otherwise stated in the announcement. </p>
                                        <p> You will be deemed to have accepted any changes made to the Terms of Service unless you notify us otherwise before they come into force. If you disagree with the proposed changes, you reserve the right to terminate and close your account immediately without charge before the proposed date of effect. </p>
                                    </div>

                                    <div class="col-sm-10 col-md-10 text-left">
                                        <h5><b>Monapay Service</b></h5>
                                        <p> Monapay (a product owned by Games Laboratories) is a licenced value-added service provider and regulated by the Nigerian Communications Commission in the Federal Republic of Nigeria. </p>
                                        <p> By accepting this Terms of Service and using the monapay Service, you acknowledge that accounts are not insured by any government agency; and we do not act as a trustee or escrow holder in respect of your account balances. </p>
                                    </div>

                                    <div class="col-sm-10 col-md-10 text-left">
                                        <h5><b>Membership</b></h5>
                                        <ol>
                                            <p>To become a user, you must:</p>
                                            <li>
                                                <p>1. Be at least 13 years old</p>
                                            </li>
                                            <li>
                                                <p>2. Not be a resident of any country where monapay Service is not provided. Monapay is currently available to users in Nigeria only – this may be updated from time to time, and without notice.</p>
                                            </li>
                                            <li>
                                                <p>3. Open an account with the instructions set out in the registration page of our “Sign Up” portal including completing all requested information set out on said page.</p>
                                            </li>
                                            <li>
                                                <p>4. Maintain an active phone number, and email address.</p>
                                            </li>
                                            <li>
                                                <p>5. Meet all requirements for identity and security validation and verification.</p>
                                            </li>
                                        </ol>
                                        <br>
                                        <ol start="2">
                                            <li>
                                                <p>You will update your account details on the website if your name, phone number, email address or bank account information change.</p>
                                            </li>
                                        </ol>
                                    </div>

                                    <div class="col-sm-10 col-md-10 text-left">
                                        <h5><b>Eligibility</b></h5>
                                        <p>To use the monapay Service, you must:</p>
                                        <ol>
                                            <p>To become a user, you must:</p>
                                            <li>
                                                <p>1. Be a registered user.</p>
                                            </li>
                                            <li>
                                                <p>2. Not be in breach of any Terms of Service.</p>
                                            </li>
                                            <li>
                                                <p>3. Not have your account restricted or previously closed by us.</p>
                                            </li>
                                        </ol>
                                    </div>

                                    <div class="col-sm-10 col-md-10 text-left">
                                        <h5><b>Account</b></h5>
                                        <ol>
                                            <li>
                                                <p>In order to complete a transaction, you will need to fund your monapay wallet using airtime from your phone. You can only spend the value of the funds that you have in your wallet.</p>
                                            </li>
                                        </ol>
                                        <ol>
                                            <li>
                                                <p>1. Purchasing digital goods</p>
                                            </li>
                                            <ol>
                                                <li>
                                                    <p>You may purchase digital goods or content by paying with monapay wallet. You must provide the information required and pass all verification checks. For deposits, you authorize us to obtain and/or receive funds on your behalf from your airtime, and issue the funds into your account.</p>
                                                </li>
                                            </ol>
                                            <li>
                                                <p>2. Your Wallet</p>
                                            </li>
                                        </ol>
                                        <p>All transactions made through your wallet are subject to:</p>
                                        <ul>
                                            <li>
                                                <p>i. Sufficient funds in your wallet</p>
                                            </li>
                                            <li>
                                                <p>The merchant and your mobile network operator being able to verify that you have sufficient funds in your wallet and on your mobile phone, respectively</p>
                                            </li>
                                        </ul>
                                        <ol start="4">
                                            <li>
                                                <p>You are solely responsible for the goods and services bought by you through monapay Services. Any dispute with a merchant regarding any product or service bought by you through the monapay Service is between you and the merchant and you agree that we shall not be a party to such dispute and will not provide any warranties, representations, conditions or guarantees with respect to such goods and services.</p>
                                            </li>
                                            <li>
                                                <p><b>Subscription Billing</b></p>
                                            </li>
                                            <ol>
                                                <li>
                                                    <p>Some online Merchants may offer goods or services which can be paid for using Subscription Billing. This means that a payment will be deducted from your account at regular intervals. The amount of the Payment and the intervals at which the Payment will be deducted is determined by the relevant Merchant.</p>
                                                </li>
                                                <li>
                                                    <p>Subscription Billing is an arrangement between you and the relevant Merchant. If you wish to amend or cancel your Subscription Billing Payment or have any query or dispute concerning your Subscription Billing Payment, you may only do this by contacting the Merchant directly and the terms and conditions set by the Merchant will apply. Once you have contacted the Merchant you should inform us. We are unable to cancel or amend Subscription Billing Payments without the consent of the Merchant. You should not cancel or otherwise reverse a Subscription Billing payment without contacting the Merchant first. We will not be liable for any Subscription Billing Payment that is deducted from your account before you have notified the Merchant of the cancellation. </p>
                                                </li>
                                                <li>
                                                    <p>It is your responsibility to ensure that your account has sufficient funds to make each of the payments you have agreed to make using Subscription Billing. We shall not be liable for any fees, charges or fines you may incur as a result of insufficient funds in your account to meet your obligations under a Subscription Billing arrangement.</p>
                                                </li>
                                            </ol>
                                        </ol>
                                    </div>

                                    <div class="col-sm-10 col-md-10 text-left">
                                        <h5><b>Rewards and Promotions</b></h5>
                                        <p> We may offer reward programs or other promotional programs which will be subject to the program rules. We reserve the right to cancel or amend the terms and duration of any such reward or promotional program at our discretion. </p>
                                    </div>

                                    <div class="col-sm-10 col-md-10 text-left">
                                        <h5><b>Account Restrictions</b></h5>
                                        <ol>
                                            <li>
                                                <p>You agree to use your account in accordance with the provisions of these Terms of Service and any other instruction we may reasonably give you regarding the use of the monapay Service.&nbsp;</p>
                                            </li>
                                        </ol>
                                    </div>

                                    <div class="col-sm-10 col-md-10 text-left">
                                        <h5><b>Keeping your account safe</b></h5>
                                        <ol>
                                            <li>
                                                <p>You must take all reasonable precautions to keep your Account Information confidential and secure. This includes ensuring the ongoing security of your Account Information and your device for accessing the Internet. You are required to change your password/PIN regularly and to use updated virus, malware and spyware scanning software and firewall protection to reduce the risk of a security breach.</p>
                                            </li>
                                        </ol>
                                    </div>

                                    <div class="col-sm-10 col-md-10 text-left">
                                        <h5><b>Warranties, Liabilities and Disclaimers</b></h5>
                                        <ol>
                                            <li>
                                                <p>We reserve the right to validate and verify any of the information that you provide to us with third parties at any time.</p>
                                            </li>
                                            <li>
                                                <p>We make no express warranties, guaranties or conditions to you with respect to the monapay Service except as set out in these Terms of Service, and all implied and statutory warranties and conditions, including without limitation any warranty or condition of fitness for a particular purpose are hereby expressly disclaimed except where implied or statutory warranties cannot be disclaimed by applicable law.</p>
                                            </li>
                                            <li>
                                                <p>Neither we nor any of our affiliates, holding companies, subsidiaries, agents or subcontractors shall be responsible for any claim, loss or damage suffered or incurred by you or any third party unless it has been caused as a direct result of our negligence or wilful misconduct; provided that under no circumstances shall we, our affiliates, holding companies, subsidiaries, agents or subcontractors be liable for any claim, loss or damage caused or alleged to be caused by any of the following:</p>
                                            </li>
                                            <ol>
                                                <li>
                                                    <p>Your breach of these Terms of Service;</p>
                                                </li>
                                                <li>
                                                    <p>Errors made by you or any Merchant or other Member in making any Transaction, such as making a payment to an unintended person or transferring an incorrect amount;</p>
                                                </li>
                                                <li>
                                                    <p>Use of your Account by another person who passes all identity and security validation and verification checks;</p>
                                                </li>
                                                <li>
                                                    <p>Failure by you to use up to date virus, malware and spyware scanning software and firewall protection on the computer or other device you use to access the Internet and failure to remove viruses, malware or spyware from any computer or device as soon as practicable after they have been found;</p>
                                                </li>
                                                <li>
                                                    <p>If you reveal your log-in information, or Card details to any third party as a result of a phishing communication;</p>
                                                </li>
                                                <li>
                                                    <p>Any fraud or misrepresentation made by a Merchant or Member, even if the Merchant or Member passes all identity and security validation and verification checks;</p>
                                                </li>
                                                <li>
                                                    <p>Circumstances beyond our reasonable control;</p>
                                                </li>
                                                <li>
                                                    <p>Any abnormal and unforeseeable circumstances beyond our control</p>
                                                </li>
                                                <li>
                                                    <p>You agree to indemnify us, our affiliates, holding companies, subsidiaries, agents and subcontractors from and against any and all claims brought by third parties against us, our affiliates, holding companies, subsidiaries, agents or subcontractors relating to your use of the monapay Service in respect of all claims, losses, damages, expenses and liabilities whatsoever suffered or incurred by us, our affiliates, holding companies, subsidiaries, agents or subcontractors as a result of your breach of these Terms of Service. This provision shall survive termination of the relationship between you and us.</p>
                                                </li>
                                            </ol>
                                        </ol>
                                    </div>

                                    <div class="col-sm-10 col-md-10 text-left">
                                        <h5><b>Rights</b></h5>
                                        <p> We confirm that monapay retains all right, title, and interest in and to all trademarks, trade names, logos, website designs, text, content and graphics, and other intellectual property rights used by us in relation to the monapay Service and any use, reproduction, modification, or distribution by you of such trademarks, trade names, logos, website designs, text, content, graphics, or other intellectual property rights, is prohibited. </p>
                                    </div>

                                    <div class="col-sm-10 col-md-10 text-left">
                                        <h5><b>Notices</b></h5>
                                        <ol>
                                            <li>
                                                <p>We may engage the services of one or more affiliates, holding companies, subsidiaries, agents or subcontractors to fulfil our obligations.</p>
                                            </li>
                                            <li>
                                                <p>This Terms of Service shall be governed by the laws of the Federal Republic of Nigeria. All disputes arising out of or relating to these Terms of Service shall be resolved by the Courts of the Federal Republic of Nigeria.</p>
                                            </li>
                                            <li>
                                                <p>We may send communications and notices to you at the email address or postal address you provided to us during the registration process (or as updated subsequently by you). Any and all communications and notices by either party under these Terms of Service by email shall be deemed given on the day the email is sent, unless the sending party receives an electronic indication that the email was not delivered.</p>
                                            </li>
                                            <li>
                                                <p>This Terms of Service are subject to amendment, modification or deletion if required by, or found in conflict with, applicable law or regulation, without affecting the validity or enforceability of the remaining Terms of Service.</p>
                                            </li>
                                            <li>
                                                <p>Our delay or failure to exercise or enforce any right under these Terms of Service shall not be deemed to be a waiver of any such right or operate to bar the exercise or enforcement thereof at any time or times thereafter.</p>
                                            </li>
                                            <li>
                                                <p>The rights and remedies available to us in this Terms of Service are cumulative and are in addition to any other right or remedy available to us at law or in equity.</p>
                                            </li>
                                            <li>
                                                <p>No provision in these Terms of Service creates a partnership between you and us or makes either of us or you the agent of the other for any purpose. You have no authority to bind, to contract in the name of, or to create liability for us in any way for any purpose.</p>
                                            </li>
                                            <li>
                                                <p>Perchance, any part of these Terms of Service is held not to be enforceable, this shall not affect the remainder of these Terms of Service which shall remain in full force and effect.</p>
                                            </li>
                                            <li>
                                                <p>All provisions of the Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability.</p>
                                            </li>
                                        </ol>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </section>

        </div>
    </div>

    <?php include("footer.php"); ?>