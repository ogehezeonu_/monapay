<?php
$mainpath = "/";
?>
    <!doctype html>
    <html lang="en">

    <head>
        <!-- COMMON TAGS -->
        <meta charset="utf-8">
        <title>Help and Support - Our support is hosted in-house</title>
        <!-- Search Engine -->
        <meta name="description" content="Get quick answers to your questions.">
        <!-- Schema.org for Google -->
        <meta itemprop="name" content="FAQs - Our support is hosted in-house">
        <meta itemprop="description" content="Get quick answers to your questions.">
        <!-- Twitter -->
        <meta name="twitter:card" content="summary">
        <meta name="twitter:title" content="FAQs - Our support is hosted in-house">
        <meta name="twitter:description" content="Get quick answers to your questions.">
        <!-- Open Graph general (Facebook, Pinterest & Google+) -->
        <meta name="og:title" content="FAQs - Our support is hosted in-house">
        <meta name="og:description" content="Get quick answers to your questions.">
        <meta name="og:image" content="https://monapay.com/img/opengraph.png">
        <meta name="og:url" content="https://monapay.com">
        <meta name="og:site_name" content="Monapay Site">
        <meta name="og:type" content="website">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/stack-interface.css" rel="stylesheet" type="text/css" media="all">
        <link href="css/socicon.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/flickity.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet" type="text/css">
        <link href="css/font-roboto.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="css/devices.min.css" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="css/index-codepanel.css">
        <link rel="stylesheet" href="css/default-codepanel.css">
        <link rel="stylesheet" href="css/prism_light.css">
        <script defer src="js/default-codepanel.js"></script>
        <script defer src="js/index-codepanel.js"></script>
        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="img/Monapay_favicon.png">
        <link rel="icon" type="image/png" href="img/Monapay_favicon.png" sizes="32x32">
    </head>
    <style type="text/css">
        .hide-it {
            display: none !important;
        }

    </style>

    <body class="scroll-assist" data-smooth-scroll-offset="77">
        <a id="start"></a>
        <div class="nav-container">
            <div class="via-1491986892298" via="via-1491986892298" vio="monapay">
                <div class="bar bar--sm visible-xs">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-8 col-sm-2">
                                <a href="<?= $mainpath ?>home.php"> <img class="logo logo-dark" alt="logo" src="img/Asset%201monapay%20logo.svg"> <img class="logo logo-light" alt="logo" src="img/Asset%201monapay%20logo.svg"> </a>
                            </div>
                            <div class="col-xs-12 col-sm-5 text-right">
                                <a href="#" class="hamburger-toggle" data-toggle-class="#menu1;hidden-xs hidden-sm"> <i class="material-icons">menu</i> </a>
                            </div>
                        </div>
                    </div>
                </div>
                <nav id="menu1" class="bar bar--sm bar-1 hidden-xs" data-scroll-class="10vh:pos-fixed">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-2 col-sm-2 hidden-xs header1">
                                <div class="bar__module">
                                    <a href="<?= $mainpath ?>home.php"> <img class="logo logo-dark" alt="logo" src="img/Asset%201monapay%20logo.svg"> <img class="logo logo-light" alt="logo" src="img/Asset%201monapay%20logo.svg"> </a>
                                </div>
                            </div>
                            <div class="col-md-11 col-sm-12 text-right text-left-xs text-left-sm header2">
                                <div class="bar__module">
                                    <ul class="menu-horizontal text-left">
                                        <li> <a href="<?= $mainpath ?>home.php#how-it-works" class="inner-link" target="_self">
                                        How it Works
                                    </a> </li>
                                        <li> <a href="<?= $mainpath ?>home.php#features" class="inner-link" target="_self">
                                        Features
                                    </a> </li>
                                        <li> <a href="<?= $mainpath ?>home.php#developers" class="inner-link" target="_self">Developers</a>
                                        </li>
                                        <li> <a href="http://www.maliyo.com">
                                        Marketplace
                                    </a> </li>
                                    </ul>
                                </div>
                                <div class="bar__module">
                                    <div class="modal-instance sgn-up">
                                        <a class="btn btn--sm modal-trigger sign__up" href="#"> <span class="btn__text">
                            Demo Account
                        </span> </a>
                                        <div class="modal-container modal" data-modal-index="4">
                                            <div class="modal-content">
                                                <section class="unpad ">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-sm-6 col-md-offset-3 col-sm-offset-3 col-xs-12">
                                                                <div class="feature feature-1">
                                                                    <div class="feature__body boxed boxed--lg boxed--border text-center">
                                                                        <div class="modal-close modal-close-cross"></div>
                                                                        <!-- Demo Account form --->
                                                                        <div>
                                                                            <div class="text-block">
                                                                                <h3>Request for demo account</h3>
                                                                                <p>
                                                                                    As a developer, you can request for demo account and testing parameters.
                                                                                </p>
                                                                            </div>
                                                                            <?php include ("requestform.phtml"); ?>
                                                                            <!-- end of demo account form -->
                                                                            <!-- flip forms
                                                                                            <div id="login">
                                                                                                <div class="text-block">
                                                                                                    <h3>Login to your account</h3>
                                                                                                    <p>
                                                                                                        Welcome back, sign in with your existing account credentials.
                                                                                                    </p>
                                                                                                </div>
                                                                                                <form>
                                                                                                    <div class="row">
                                                                                                        <div class="col-sm-12">
                                                                                                            <input type="email" name="Email Address" placeholder="Email Address">
                                                                                                        </div>
                                                                                                        <div class="col-sm-12">
                                                                                                            <input type="password" placeholder="Password">
                                                                                                        </div>
                                                                                                        <div class="col-sm-12">
                                                                                                            <button class="btn btn--primary type--uppercase" type="submit"><span class="btn__text">Login</span></button>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                </form>
                                                                                                <span class="type--fine-print block">Don't have an account yet? <a href="#" id="flip-login">Create account</a></span> <span class="type--fine-print block">Forgot your username or password? <a href="#" id="flip-recover">Recover account</a></span>
                                                                                            </div>

                                                                                            <div class="hidden" id="recover-account">
                                                                                                <div class="row">
                                                                                                    <div class="text-block">
                                                                                                        <h3>Recover your account</h3>
                                                                                                        <p> Enter email address to send a recovery email. </p>
                                                                                                        <form>
                                                                                                            <div class="col-sm-12">
                                                                                                                <input type="email" placeholder="Email Address"></div>
                                                                                                            <div class="col-sm-12">
                                                                                                                <button class="btn btn--primary type--uppercase" type="submit"><span class="btn__text">Recover Account</span></button>
                                                                                                            </div>
                                                                                                        </form>
                                                                                                        <span class="type--fine-print block">Dont have an account yet? <a href="#" id="flip-signup-recover">Create account</a></span>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="hidden" id="signup">
                                                                                                <div class="row">
                                                                                                    <div class="text-block">
                                                                                                        <h3>Sign up and start earning with monapay</h3>
                                                                                                        <p>Get started with monapay today, No credit card required.</p>
                                                                                                        <form>
                                                                                                            <div class="row">
                                                                                                                <div class="col-sm-12"> <input type="email" name="Email Address" placeholder="Email Address"> </div>
                                                                                                                <div class="col-sm-12"> <input type="password" name="Password" placeholder="Password"> </div>
                                                                                                                <div class="col-sm-12"> <button type="submit" class="btn btn--primary type--uppercase"><span class="btn__text">Create Account</span></button> </div>
                                                                                                                <div class="col-sm-12"> <span class="type--fine-print">By signing up, you agree to the <a href="terms-of-service.html">Terms of Service</a></span> </div>
                                                                                                            </div>
                                                                                                        </form>
                                                                                                        <span class="type--fine-print block">Already a user? <a href="#" id="flip-signup">Login</a></span>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>-->

                                                                        </div>
                                                                    </div>
                                                                    <!--end feature-->
                                                                </div>
                                                            </div>
                                                            <!--end of row-->
                                                        </div>
                                                        <!--end of container-->
                                                </section>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </nav>
                </div>
            </div>
            <div class="main-container">
                <div class="main-content">
                    <section class="text-center imagebg space--md" data-overlay="5">
                        <div class="background-image-holder"><img alt="background" src="img/help-support-purple.jpg"></div>
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-10 col-md-8">
                                    <h1 class="white-text">Help and Support</h1>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="text-center bg--grey" style="padding-top:0;">
                        <section class="text-center">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-7 col-md-6">
                                        <p class="lead">
                                            Our support is hosted in-house. Who would you rather speak to than one who knows the product intimately?
                                        </p>
                                    </div>
                                </div>
                                <!--end of row-->
                            </div>
                            <!--end of container-->
                        </section>
                        <div class="background-image-holder"> </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="feature feature-3 boxed boxed--border boxed--pd"> <img src="img/icon-consumers.svg" width="60px" height="60px">
                                        <h4>For Customers</h4>
                                        <p> You can search for an answer in the FAQs or you Contact us. </p>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="feature feature-3 boxed boxed--border boxed--pd"> <img src="img/icon-merchants.svg" width="60px" height="60px">
                                        <h4>For Merchants</h4>
                                        <p> General Information and how to start earning with monapay.</p>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="feature feature-3 boxed boxed--border boxed--pd"> <img src="img/icon-developers.svg" width="60px" height="60px">
                                        <h4>For developers</h4>
                                        <p> Technical oveview of our payment product and how it is integrated. </p>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <a href="<?= $mainpath ?>direct-carrier-billing.php">More on Direct billing with monapay</a>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="text-center">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3>Frequently Asked Questions</h3>
                                    <p class="lead">Get quick answers to your questions. Contact Us if you do not find what you're looking for.</p>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="text-block">
                                        <h4>Merchant FAQs</h4>
                                    </div>
                                </div>
                                <div class="col-sm-9">
                                    <ul class="accordion accordion-2">
                                        <div class="col-sm-6">
                                            <div class="text-block">
                                                <li>
                                                    <div class="accordion__title"> <span class="h5">How long does integration take?</span> </div>
                                                    <div class="accordion__content">
                                                        <p> Integration takes less than 24 hours. The API is available on our website. </p>
                                                    </div>
                                                </li>
                                            </div>
                                            <div class="text-block">
                                                <li>
                                                    <div class="accordion__title"> <span class="h5">Can I get help with integration?</span> </div>
                                                    <div class="accordion__content">
                                                        <p> Should you require additional support with integration, our support team will be on hand to help. Please contact us here (link to support page)</p>
                                                    </div>
                                                </li>
                                            </div>
                                            <div class="text-block">
                                                <li>
                                                    <div class="accordion__title"> <span class="h5">How can I pay out funds from my account?</span> </div>
                                                    <div class="accordion__content">
                                                        <p> Merchant payouts are done every 45 days. Once your successful transactions for a month have been settled and funded in your merchant wallet, you will be able to pay out. Funds will be transferred to your pre-registered bank account.</p>
                                                    </div>
                                                </li>
                                            </div>
                                            <div class="text-block">
                                                <li>
                                                    <div class="accordion__title"> <span class="h5">What is the payout cycle duration?</span> </div>
                                                    <div class="accordion__content">
                                                        <p> The payout cycle is 45 days, subject to settlement with the networks.</p>
                                                    </div>
                                                </li>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="text-block">
                                                <li>
                                                    <div class="accordion__title"> <span class="h5">How do I see my payout history?</span> </div>
                                                    <div class="accordion__content">
                                                        <p> Your payout history can be viewed from your merchant wallet. </p>
                                                    </div>
                                                </li>
                                            </div>
                                            <div class="text-block">
                                                <li>
                                                    <div class="accordion__title"> <span class="h5">Why can’t I pay out funds from my account?</span> </div>
                                                    <div class="accordion__content">
                                                        <p> If you are having challenges paying out from your merchant wallet, check to be sure that your payment cycle is due and that funds have been credited to your wallet. If all these are in place and you are unable to pay out, kindly contact support@monapay.com </p>
                                                    </div>
                                                </li>
                                            </div>
                                            <div class="text-block">
                                                <li>
                                                    <div class="accordion__title"> <span class="h5">How do I change my password?</span> </div>
                                                    <div class="accordion__content">
                                                        <p> To change your merchant password, click on “My Account” on your merchant dashboard; select “Change Password” from the drop down list. A password reset link will be sent to your registered email address. </p>
                                                    </div>
                                                </li>
                                            </div>
                                            <div class="text-block">
                                                <li>
                                                    <div class="accordion__title"> <span class="h5">Can I change my bank account details?</span> </div>
                                                    <div class="accordion__content">
                                                        <p> Yes, merchants are allowed to change their bank account details. Go to “My Account” on your merchant dashboard, select “Bank Account” from the dropdown list. Please note that it takes 10 working days to effect this change. </p>
                                                    </div>
                                                </li>
                                            </div>
                                        </div>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                    <a id="#customer-faq" class="in-page-link" data-scroll-id="customer-faq"></a>
                    <section class="bg--grey height-auto">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="text-block">
                                        <h4>Customer FAQs</h4>
                                    </div>
                                </div>
                                <div class="col-sm-9">
                                    <ul class="accordion accordion-2">
                                        <div class="col-sm-6">
                                            <div class="text-block">
                                                <li>
                                                    <div class="accordion__title"> <span class="h5">How do I create an account?</span> </div>
                                                    <div class="accordion__content">
                                                        <p> When you are trying to buy digital goods from any of our registered merchants, you will be prompted to create an account from the app that you are using. You need a valid telephone number, email address and a 4-digit PIN to create an account.</p>
                                                    </div>
                                                </li>
                                            </div>
                                            <div class="text-block">
                                                <li>
                                                    <div class="accordion__title"> <span class="h5">How do I change my PIN?</span> </div>
                                                    <div class="accordion__content">
                                                        <p> PIN codes can be reset from the website. After logging in to your customer page, click on “My Account” and select “Change PIN” from the dropdown list. You will be required to input your existing PIN and your new PIN.</p>
                                                    </div>
                                                </li>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="text-block">
                                                <li>
                                                    <div class="accordion__title"> <span class="h5">How do I add or delete a number?</span> </div>
                                                    <div class="accordion__content">
                                                        <p>After logging into your account, you can click on the “Add/Delete Number” to add or delete a number. Your account must have at least one mobile number at all times, so you will not be able to delete a&nbsp;number except you have more than one.&nbsp;<br>You can also change your default number. The number you used at the point of registration is the default number.</p>
                                                    </div>
                                                </li>
                                            </div>
                                            <div class="text-block">
                                                <li>
                                                    <div class="accordion__title"> <span class="h5">How do I fund my wallet?</span> </div>
                                                    <div class="accordion__content">
                                                        <p> You can fund your MonaPay wallet directly via USSD or short code.</p>
                                                    </div>
                                                </li>
                                            </div>
                                        </div>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </section>
                </div>
                <a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
                    <i class="stack-interface stack-up-open-big"></i>
                </a>
            </div>

            <?php include("footer.php"); ?>
