<?php include("header.php"); ?>
        <div class="main-content">
            <section class="text-center imagebg space--md" data-overlay="5">
                <div class="background-image-holder"><img alt="background" src="img/privacy-policy-purple.jpg"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-10 col-md-8">
                            <h1 class="white-text">Privacy Policy</h1>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="container">
                    <div class="row flex">
                        <div class="col-sm-6">
                            <nav class="nav-sidebar">
                                <ul class="nav">

                                    <li><a href="<?= $mainpath ?>terms-of-service.php" target="_self">Terms of Service</a><br></li>
                                    <li><a href="<?= $mainpath ?>privacy-policy.php" class="active" target="_self">Privacy policy</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-sm-10">
                            <div class="container">
                                <div class="row">
                                    <!-- content for page goes here -->
                                    <div class="col-sm-10 col-md-10 text-left tp">
                                        <p> This Privacy Policy describes your privacy rights regarding our collection, use, storage, sharing and protection of your personal information, as it applies to use on our website and other services (monapay Services). These include the monapay website and all related sites, content, applications, services and tools regardless of how you access or use them.</p>
                                    </div>
                                    <div class="col-sm-10 col-md-10 text-left">
                                        <h5><b>Scope and Consent</b></h5>

                                        <p>You accept this Privacy Policy when you sign up for, access, or use our products, services, content, features, technologies or functions offered on our website and all related sites, applications, and services. We may amend this Privacy Policy at any time by posting a revised version on our website. The revised version will be effective as of the published effective date. </p>

                                    </div>
                                    <div class="col-sm-10 col-md-10 text-left">
                                        <h5><b>Collection of Personal Information</b></h5>
                                        <p> We collect the following types of personal information in order to provide you with the use of monapay Services, and to help us personalize and improve your experience. </p>
                                    </div>
                                    <div class="col-sm-10 col-md-10 text-left">
                                        <h5><b>Information we collect automatically:</b></h5>
                                        <p> When you use monapay Services, we collect information sent to us by your computer, mobile phone or other access device. The information we receive includes, but is not limited to, the following: computer IP address, device ID, device type, geo-location information, computer information, mobile network information, statistics on page views, traffic to and from the sites, referral URL, ad data, and standard web log data and other information. We also collect anonymous information through our use of cookies. </p>
                                    </div>

                                    <div class="col-sm-10 col-md-10 text-left">
                                        <h5><b>Information you provide:</b></h5>
                                        <p>We may collect and store any information you provide us when you use monapay Services, including your phone number and email address. When you use monapay Services, we also collect information about your transactions and your activities. In addition, if you open a monapay account or use monapay Services, we may collect the following types of information:
                                            <ul>
                                                <li>
                                                    <p><b>Contact information</b> - phone number, email, bank account details (for merchants) and other similar information.</p>
                                                </li>
                                                <li>
                                                    <p><b>Financial information</b> – phone number (for airtime payments) and full bank account details for merchants payouts.</p>
                                                </li>
                                            </ul>
                                            We may also collect information from or about you from other sources, such as through your contact with us, including our customer support team, your results when you respond to a survey, and from other accounts we have reason to believe you control. By communicating with monapay, you acknowledge that your communication may be overheard, monitored, or recorded without further notice or warning.</p>
                                    </div>
                                    <div class="col-sm-10 col-md-10 text-left">
                                        <h5><b>Information from other sources:</b></h5>
                                        <p>You may choose to provide us with access to certain personal information stored by third parties such as your mobile network provider or social networking sites. The information we may receive varies by network and is controlled by the network provider in question. By associating an account managed by a third party with your monapay account and authorizing monapay to have access to this information, you agree that monapay may collect, store and use this information in accordance with this Privacy Policy.</p>
                                    </div>


                                    <div class="col-sm-10 col-md-10 text-left">
                                        <h5><b>Authentication and Fraud Detection:</b></h5>
                                        <p>In order to help protect you from fraud and misuse of your personal information, we may collect information about you and your interactions with monapay Services.</p>
                                    </div>


                                    <div class="col-sm-10 col-md-10 text-left">
                                        <h5><b>Using Your Mobile Device:</b></h5>
                                        <p> We may offer you the ability to connect with monapay Services using a mobile device, either through a mobile application or via a mobile optimized website. The provisions of this Privacy Policy apply to all such mobile access and use of mobile devices. </p>
                                        <p>When you use our mobile applications, or access one of our mobile optimized sites, we may receive information about your location and your mobile device, including a unique identifier for your device. We may use this information to provide you with location-based services, such as advertising, or other personalized or recommended content. Most mobile devices allow you to manage your location services in the device's ‘settings’ menu. </p>
                                    </div>

                                    <div class="col-sm-10 col-md-10 text-left">
                                        <h5><b>How We Use the Personal Information We Collect</b></h5>
                                        <p>Our primary purpose in collecting personal information is to provide you with a secure, seamless and customized experience. We may use your personal information to:</p>

                                        <ol>
                                            <li>
                                                <p>
                                                    1. Provide monapay Services and customer support;
                                                </p>

                                            </li>
                                            <li>
                                                <p>
                                                    2. Process transactions and send notifications about your transactions;
                                                </p>

                                            </li>
                                            <li>
                                                <p>
                                                    3. Verify your identity, including during account creation and password/PIN reset processes;
                                                </p>

                                            </li>
                                            <li>
                                                <p>
                                                    4. Manage risk, or to detect and prevent fraud;
                                                </p>

                                            </li>
                                            <li>
                                                <p>
                                                    5. Provide targeted advertising, provide service update notices, and deliver offers through your preferred communication channels;
                                                </p>

                                            </li>
                                            <li>
                                                <p>
                                                    6. Contact you at any telephone number, by placing a voice call or through text (SMS) or email messaging, as authorized by our User Agreement;
                                                </p>

                                            </li>
                                            <li>
                                                <p>
                                                    7. Compare information for accuracy and verify it with third parties.
                                                </p>

                                            </li>
                                        </ol>
                                        <p>We may contact you as necessary to enforce our policies, applicable law, or any agreement we may have with you. Where applicable and permitted by law, you may decline to receive certain communications.</p>
                                    </div>


                                    <div class="col-sm-10 col-md-10 text-left">
                                        <h5><b>Marketing</b></h5>
                                        <ol>
                                            <li>
                                                <p>We do not sell or rent your personal information to third parties for their marketing purposes without your consent. We may combine your information with information we collect from other companies and use it to improve and personalize monapay Services, content, and advertising.</p>
                                            </li>
                                        </ol>
                                    </div>

                                    <div class="col-sm-10 col-md-10 text-left">
                                        <h5><b>Use of Cookies</b></h5>
                                        <p>When you access our website or use monapay Services, we may place small data files such as Cookies on your computer, mobile phone or other device. We use these technologies to recognize you as a customer; customize monapay Services, content, and advertising; measure promotional effectiveness; help ensure that your account security is not compromised; mitigate risk and prevent fraud; and to promote trust and safety across our sites and monapay Services.</p>
                                        <p>You are free to decline our Cookies if your browser or browser add-on permits, unless our Cookies are required to prevent fraud or ensure the security of websites we control. However, declining our Cookies may interfere with your use of our website and monapay Services.</p>
                                    </div>

                                    <div class="col-sm-10 col-md-10 text-left">
                                        <h5><b>Protection and Storage of Personal Information</b></h5>
                                        <p> Throughout this Privacy Policy, we use the term "personal information" to describe information that can be associated with a specific person and can be used to identify that person. We do not consider personal information to include information that has been made anonymous so that it does not identify a specific user. </p>
                                        <p>We protect your information using physical, technical, and administrative security measures to reduce the risks of loss, misuse, unauthorized access, disclosure and alteration. Some of the safeguards we use are firewalls and data encryption, physical access controls to our data centres, and information access authorization controls.</p>
                                    </div>

                                    <div class="col-sm-10 col-md-10 text-left">
                                        <h5><b>How We Share Personal Information with Other monapay Users</b></h5>
                                        <p>When transacting with others, we may provide those parties with information about you necessary to complete the transaction, such as your name, account ID, contact details, or other information needed to promote the reliability and security of the transaction. If a transaction is held, fails, or is later invalidated, we may also provide details of the unsuccessful transaction.</p>
                                        <p>We work with third parties, including merchants, to enable them to accept or send payments from or to you using monapay. In doing so, a third party may share information about you with us, such as your email address or mobile phone number, to inform you that a payment has been made to you or when you attempt to pay a merchant or third party. We use this information to confirm that you are a monapay customer and that monapay as a form of payment can be enabled, or to send you notification of payment status. </p>
                                        <p>Please note that merchants, sellers, and users you buy from or contract with have their own privacy policies, and although monapay’s User Agreement does not allow the other transacting party to use this information for anything other than providing monapay Services, monapay is not responsible for their actions, including their information protection practices.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-10 col-md-10 text-left">
                                <h5><b>Sharing Personal Information with Third Parties</b></h5>
                                <p>We may share your personal information with:</p>

                                <ol>
                                    <li>
                                        <p>
                                            1. Members of the monapay corporate family to help detect and prevent potentially illegal acts and violations of our policies, and to guide decisions about their products, services, and communications. Members of our corporate family will use this information to send you marketing communications only if you have requested their services.
                                        </p>

                                    </li>
                                    <li>
                                        <p>
                                            2. Companies that we plan to merge with or are acquired by.
                                        </p>

                                    </li>
                                    <li>
                                        <p>
                                            3. Law enforcement, government officials, or other third parties pursuant to a legal process or requirement applicable to monapay or one of its affiliates; when we need to do so to comply with law.
                                        </p>
                                    </li>

                                </ol>
                                <p>Other unaffiliated third parties, for the following purposes:</p>

                                <ol>
                                    <li>
                                        <p>
                                            1. Customer Service: for customer service purposes, including to help service your accounts or resolve disputes (e.g., billing or transactional).
                                        </p>

                                    </li>
                                    <li>
                                        <p>
                                            2. Content/Service Providers: to enable content and service providers under contract with us to support our business operations. Our contracts dictate that these content and service providers only use your information in connection with the services they perform for us and not for their own benefit.
                                        </p>

                                    </li>
                                </ol>
                                <p>Other third parties with your consent or direction to do so.</p>

                                <p>If you open a monapay account directly on a third party website or via a third party application, any information that you enter on that website or application (and not directly on a monapay website) will be shared with the owner of the third party website or application. These sites are governed by their own privacy policies and you are encouraged to review their privacy policies before providing them with personal information. Monapay is not responsible for the content or information practices of such third parties.</p>
                            </div>
                            <div class="col-sm-10 col-md-10 text-left">
                                <h5><b>Cross Border Transfers of Personal Information</b></h5>
                                <p>Monapay is committed to adequately protecting your personal information regardless of where the data resides and to providing appropriate protection for your personal information where such data is transferred across borders, including outside of SSA.</p>
                            </div>

                            <div class="col-sm-10 col-md-10 text-left">
                                <h5><b>How You Can Access or Change Your Personal Information</b></h5>
                                <p>You can review and edit your personal information at any time by logging into your account and reviewing your account settings and profile. If you would like to limit the processing of your personal data, please feel free to contact us at the contact details as provided below. Please note that all information requested from you is obligatory to be provided by you unless stated otherwise. Should you fail to provide us with your personal data, we may not be able to process your request for our product and / or services.</p>
                            </div>

                        </div>
                    </div>
                </div>
            </section>

        </div>
    </div>

    <?php include("footer.php"); ?>