<?php
$mainpath = "/";
?>
    <!doctype html>
    <html lang="en">

    <head>
        <!-- COMMON TAGS -->
        <meta charset="utf-8">
        <title>Developers - Easy API integration across all platforms</title>
        <!-- Search Engine -->
        <meta name="description" content="Monapay's Direct Carrier Billing payment API has a seamless integration process. Contact us if you have additional support. ">
        <!-- Schema.org for Google -->
        <meta itemprop="name" content="Developers - Easy API integration across all platforms">
        <meta itemprop="description" content="Monapay's Direct Carrier Billing payment API has a seamless integration process. Contact us if you have additional support. ">
        <!-- Twitter -->
        <meta name="twitter:card" content="summary">
        <meta name="twitter:title" content="Developers - Easy API integration across all platforms">
        <meta name="twitter:description" content="Monapay's Direct Carrier Billing payment API has a seamless integration process. Contact us if you have additional support. ">
        <!-- Open Graph general (Facebook, Pinterest & Google+) -->
        <meta name="og:title" content="Developers - Easy API integration across all platforms">
        <meta name="og:description" content="Monapay's Direct Carrier Billing payment API has a seamless integration process. Contact us if you have additional support. ">
        <meta name="og:image" content="https://monapay.com/img/opengraph.png">
        <meta name="og:url" content="https://monapay.com">
        <meta name="og:site_name" content="Monapay Site">
        <meta name="og:type" content="website">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/stack-interface.css" rel="stylesheet" type="text/css" media="all">
        <link href="css/socicon.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/flickity.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet" type="text/css">
        <link href="css/font-roboto.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="css/devices.min.css" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="css/index-codepanel.css">
        <link rel="stylesheet" href="css/default-codepanel.css">
        <link rel="stylesheet" href="css/prism_light.css">
        <script defer src="js/default-codepanel.js"></script>
        <script defer src="js/index-codepanel.js"></script>
        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="img/Monapay_favicon.png">
        <link rel="icon" type="image/png" href="img/Monapay_favicon.png" sizes="32x32">
    </head>
    <style type="text/css">
        .hide-it {
            display: none !important;
        }
    </style>

<body class="scroll-assist" data-smooth-scroll-offset="77">
<a id="start"></a>
<div class="nav-container">
    <div class="via-1491986892298" via="via-1491986892298" vio="monapay">
        <div class="bar bar--sm visible-xs">
            <div class="container">
                <div class="row">
                    <div class="col-xs-8 col-sm-2">
                        <a href="<?= $mainpath ?>home.php"> <img class="logo logo-dark" alt="logo" src="img/Asset%201monapay%20logo.svg"> <img class="logo logo-light" alt="logo" src="img/Asset%201monapay%20logo.svg"> </a>
                    </div>
                    <div class="col-xs-12 col-sm-5 text-right">
                        <a href="#" class="hamburger-toggle" data-toggle-class="#menu1;hidden-xs hidden-sm"> <i class="material-icons">menu</i> </a>
                    </div>
                </div>
            </div>
        </div>
        <nav id="menu1" class="bar bar--sm bar-1 hidden-xs" data-scroll-class="10vh:pos-fixed">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-sm-2 hidden-xs header1">
                        <div class="bar__module">
                            <a href="<?= $mainpath ?>home.php"> <img class="logo logo-dark" alt="logo" src="img/Asset%201monapay%20logo.svg"> <img class="logo logo-light" alt="logo" src="img/Asset%201monapay%20logo.svg"> </a>
                        </div>
                    </div>
                    <div class="col-md-11 col-sm-12 text-right text-left-xs text-left-sm header2">
                        <div class="bar__module">
                            <ul class="menu-horizontal text-left">
                                <li> <a href="<?= $mainpath ?>home.php#how-it-works" class="inner-link" target="_self">
                                        How it Works
                                    </a> </li>
                                <li> <a href="<?= $mainpath ?>home.php#features" class="inner-link" target="_self">
                                        Features
                                    </a> </li>
                                <li> <a href="<?= $mainpath ?>home.php#developers" class="inner-link" target="_self">Developers</a>
                                </li>
                                <li> <a href="http://www.maliyo.com">
                                        Marketplace
                                    </a> </li>
                            </ul>
                        </div>
                        <div class="bar__module">
                            <div class="modal-instance sgn-up">
                                <a class="btn btn--sm modal-trigger sign__up" href="#"> <span class="btn__text">
                            Demo Account
                        </span> </a>
                                <div class="modal-container modal" data-modal-index="4">
                                    <div class="modal-content">
                                        <section class="unpad ">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-sm-6 col-md-offset-3 col-sm-offset-3 col-xs-12">
                                                        <div class="feature feature-1">
                                                            <div class="feature__body boxed boxed--lg boxed--border text-center">
                                                                <div class="modal-close modal-close-cross"></div>
                                                                <!-- Demo Account form --->
                                                                <div>
                                                                    <div class="text-block">
                                                                        <h3>Request for demo account</h3>
                                                                        <p>
                                                                            As a developer, you can request for demo account and testing parameters.
                                                                        </p>
                                                                    </div>
                                                                    <?php include ("requestform.phtml"); ?>
                                                                    <!-- end of demo account form -->
                                                                    <!-- flip forms
                                                                                            <div id="login">
                                                                                                <div class="text-block">
                                                                                                    <h3>Login to your account</h3>
                                                                                                    <p>
                                                                                                        Welcome back, sign in with your existing account credentials.
                                                                                                    </p>
                                                                                                </div>
                                                                                                <form>
                                                                                                    <div class="row">
                                                                                                        <div class="col-sm-12">
                                                                                                            <input type="email" name="Email Address" placeholder="Email Address">
                                                                                                        </div>
                                                                                                        <div class="col-sm-12">
                                                                                                            <input type="password" placeholder="Password">
                                                                                                        </div>
                                                                                                        <div class="col-sm-12">
                                                                                                            <button class="btn btn--primary type--uppercase" type="submit"><span class="btn__text">Login</span></button>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                </form>
                                                                                                <span class="type--fine-print block">Don't have an account yet? <a href="#" id="flip-login">Create account</a></span> <span class="type--fine-print block">Forgot your username or password? <a href="#" id="flip-recover">Recover account</a></span>
                                                                                            </div>

                                                                                            <div class="hidden" id="recover-account">
                                                                                                <div class="row">
                                                                                                    <div class="text-block">
                                                                                                        <h3>Recover your account</h3>
                                                                                                        <p> Enter email address to send a recovery email. </p>
                                                                                                        <form>
                                                                                                            <div class="col-sm-12">
                                                                                                                <input type="email" placeholder="Email Address"></div>
                                                                                                            <div class="col-sm-12">
                                                                                                                <button class="btn btn--primary type--uppercase" type="submit"><span class="btn__text">Recover Account</span></button>
                                                                                                            </div>
                                                                                                        </form>
                                                                                                        <span class="type--fine-print block">Dont have an account yet? <a href="#" id="flip-signup-recover">Create account</a></span>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="hidden" id="signup">
                                                                                                <div class="row">
                                                                                                    <div class="text-block">
                                                                                                        <h3>Sign up and start earning with monapay</h3>
                                                                                                        <p>Get started with monapay today, No credit card required.</p>
                                                                                                        <form>
                                                                                                            <div class="row">
                                                                                                                <div class="col-sm-12"> <input type="email" name="Email Address" placeholder="Email Address"> </div>
                                                                                                                <div class="col-sm-12"> <input type="password" name="Password" placeholder="Password"> </div>
                                                                                                                <div class="col-sm-12"> <button type="submit" class="btn btn--primary type--uppercase"><span class="btn__text">Create Account</span></button> </div>
                                                                                                                <div class="col-sm-12"> <span class="type--fine-print">By signing up, you agree to the <a href="terms-of-service.html">Terms of Service</a></span> </div>
                                                                                                            </div>
                                                                                                        </form>
                                                                                                        <span class="type--fine-print block">Already a user? <a href="#" id="flip-signup">Login</a></span>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>-->

                                                                </div>
                                                            </div>
                                                            <!--end feature-->
                                                        </div>
                                                    </div>
                                                    <!--end of row-->
                                                </div>
                                                <!--end of container-->
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</div>
<div class="main-container">
       <style type="text/css">
        pre {
            font-size: 1rem;
            padding: .66001rem 9.5px 9.5px;
            line-height: 2rem;
            background: -webkit-linear-gradient(top, #fff 0, #fff 0.75rem, #f5f7fa 0.75rem, #f5f7fa 2.75rem, #fff 2.75rem, #fff 4rem);
            background: linear-gradient(to bottom, #fff 0, #fff 0.75rem, #f5f7fa 0.75rem, #f5f7fa 2.75rem, #fff 2.75rem, #fff 4rem);
            background-size: 100% 4rem;
            border: 1px solid #D3DAEA;
            word-wrap: break-word;
        }
        
        pre {
            display: block;
            margin: 0 0 10px;
            word-break: break-all;
            word-wrap: break-word;
            color: #333;
            background-color: #f5f5f5;
            border: 1px solid #ccc;
            border-radius: 4px;
            word-wrap: break-word;
        }
        
        .nav-sidebar {
            width: 100%;
            height: 6.5%;
            padding: 20px 20px;
            /* border: 1px solid #f7f7f7; */
            position: relative;
            background-color: #f7f7f7;
        }
        
        .nav-sidebar {
            width: 100%;
            padding: 20px 20px;
            /* border: 1px solid #f7f7f7; */
            position: relative;
            background-color: #f7f7f7;
            font-family: 'poppins' !important;
            font-weight: 700px !important;
            font-size: 12px !important;
            color: #505050 !important;
        }
        
        .nav-sidebar a {
            color: #505050;
            -webkit-transition: all 0.08s linear;
            -moz-transition: all 0.08s linear;
            -o-transition: all 0.08s linear;
            transition: all 0.08s linear;
            -webkit-border-radius: 4px 0 0 4px;
            -moz-border-radius: 4px 0 0 4px;
            border-radius: 4px 0 0 4px;
            font-weight: 700px !important;
        }
        
        .nav-sidebar .active a {
            cursor: default;
            background-color: #428bca;
            color: #888;
            text-shadow: 1px 1px 1px #666;
        }
        
        nav.nav-sidebar a:hover {
            color: #888;
        }
        
        .nav-sidebar a.active {
            color: #888;
        }
        
        .nav-sidebar .active a:hover {
            background-color: #428bca;
        }
        
        .nav-sidebar .text-overflow a,
        .nav-sidebar .text-overflow .media-body {
            white-space: nowrap;
            overflow: hidden;
            -o-text-overflow: ellipsis;
            text-overflow: ellipsis;
        }
        /* Right-aligned sidebar */
        
        .nav-sidebar.pull-right {
            border-right: 0;
            border-left: 1px solid #ddd;
        }
        
        .nav-sidebar.pull-right a {
            -webkit-border-radius: 0 4px 4px 0;
            -moz-border-radius: 0 4px 4px 0;
            border-radius: 0 4px 4px 0;
        }
    </style>

        <div class="main-content">
            <section class="text-center imagebg space--md" data-overlay="5">
                <div class="background-image-holder"><img alt="background" src="img/api-documentation-purple.jpg"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-10 col-md-8">
                            <h1 class="white-text">Documentation</h1>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="container">
                    <div class="row flex">

<div class="col-sm-6">
    <nav class="nav-sidebar">
        <ul class="nav">

            <li><a href="#merchant_params" class="inner-link" target="_self">Merchant API Detail</a><br></li>
            <li><a href="#1_Payment_Initialization" class="inner-link" target="_self">Payment Initialization</a>
                <li><a href="#Sample_Code_36" class="inner-link" target="_self">Android: Java sample</a></li>
                <li><a href="#2_iOS_ObjectiveC_62" class="inner-link" target="_self">iOS: Objective-C sample</a></li>
                <li><a href="#3_PHP_for_web_79" class="inner-link" target="_self">Web: PHP sample</a></li>
            </li>
            <li><a href="#2_Payment_Verification" class="inner-link" target="_self">Payment Verification</a><br></li>
            <li>
                <div class="modal-instance">
                    <a class="btn btn--sm modal-trigger" style="border-radius:0px;" href="#"> <span class="btn__text">
                            Demo Account
                        </span> </a>
                    <div class="modal-container modal" data-modal-index="4">
                        <div class="modal-content">
                            <section class="unpad ">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-6 col-md-offset-3 col-sm-offset-3 col-xs-12">
                                            <div class="feature feature-1">
                                                <div class="feature__body boxed boxed--lg boxed--border text-center">
                                                    <div class="modal-close modal-close-cross"></div>
                                                    <!-- Demo Account form --->
                                                    <div>
                                                        <div class="text-block">
                                                            <h3>Request for demo account</h3>
                                                            <p>
                                                                As a developer, you can request for demo account and testing parameter!
                                                            </p>
                                                        </div>
                                                        <?php include ("requestform.phtml"); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--end feature-->
                                        </div>
                                    </div>
                                    <!--end of row-->
                                </div>
                                <!--end of container-->
                            </section>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </nav>
</div>


                        <div class="col-sm-10">

                            <h4 id="merchant_params" class="scrollspy">Merchant API Detail</h4>

                            <pre><code class="language-sh wrap"><span class="hljs-comment">## Your Merchant Params</span>
Merchant Name:      Test Merchant 
Merchant ID:        <span class="hljs-number">123</span>
</code></pre>
                            <p>The AirPay API allows developers to integrate our wallet system into their applications of any platform which is capable of displaying webpage for user interactivity. This wallet can easily be funded via User airtime balance or with any credit or debit cards. International cards can also be used.</p>
                            <p>Our cross-Platform payments solution can be called from any mobile or web application on any platform, that is capable launching a browser window ( or in-app browser) - without the user even leaving the app. When detecting that user is on a mobile device, AirPay’s payment pages automatically morph themselves into slick, small-screen-optimized version of our payment flow.</p>
                            <p>Payment flow can be opened in a pop up in app browser, inside an iframe or new browser window. When calling a payment flow, different payment initialization parameters should be specified in order to make the payment experience for your users as seamless as possible.</p>

                            <h4 id="1_Payment_Initialization" class="scrollspy"><br>1. Payment Initialization</h4>
                            <p>The process is to construct a payment url with the product detail and other params, the essential params are <code>ref_id</code>, <code>merchant_id</code>, <code>key</code>, <code>uuid</code>, <code>amount</code> and <code>description</code> of the transaction. The developer will have to load the generated url in the browser, or redirect the page to the url (in case of webapp) for user to interact with.</p>
                            <h5>
                                <a id="The_generated_pay_url_example_21"></a>The generated pay url example:</h5>
                            <pre><code class="language-sh wrap"><span class="hljs-comment"># http://beta.monapay.com:80/v1/merchant/pay?reference_id=MG1489414003&amp;merchant_id=3&amp;product_key=25ab5370735e2039<br>1ce2df6329df5c1d9fa15c41&amp;uuid=456789876543456796gvbndcvbnmnbv&amp;amount=1000&amp;description=Buy+MaliyoToken5000+for+10+NGN</span>
</code></pre>
                            <h5>
                                <a id="Parameters_for_payment_url_26"></a>Parameters for payment url</h5>
                            <ul>
                                <li>[reference_id] - <code>Max length: 50</code> This is a unique transaction id generated by the developer for his/her reference purpose <code>note that this</code> kotr2017-02-08 12:58:56 <code>is not looking like a valid reference_id but to be on the safer side, rip all special characters off and use value that matches with this expression</code> A-Za-z0-9 <code>e.g:</code> kotr20170208125856</li>
                                <li>[merchant_id] - Developer merchant account id</li>
                                <li>[product_key] - Product key, <code>this is NOT the product secret key</code></li>
                                <li>[uuid] - This is user device id also known as <code>guid</code>, <code>it will be advisable to rip off all special characters</code>, in case of web app, a guid value can be generated and keep in broswer localstorage or cookie for later use, this will help in unique identification of a user or their browser.</li>
                                <li>[amount] - Amount in lowest unit, <code>kobo</code> for Nigerian currency</li>
                                <li>[description] - <code>Max length: 100</code>; Short description of the transaction</li>
                            </ul>
                            <h5>
                                <a id="Sample_Code_36"></a>Sample Code</h5>
                            <h5 id="1_Android_37" class="scrollspy">1. Android</h5>
                            <p>You can easily generate the payment url in your android code faster with the <code>URI.Builder()</code> class you can also read more on this link <a href="http://stackoverflow.com/questions/19167954/use-uri-builder-in-android-or-create-url-with-variables">Use URI builder in Android or create URL with variables</a></p>
                            <pre><code class="language-java wrap">Uri.Builder builder = <span class="hljs-keyword">new</span> Uri.Builder();
builder.scheme(<span class="hljs-string">"http"</span>)
    .authority(<span class="hljs-string">"beta.monapay.com:80"</span>)
    .appendPath(<span class="hljs-string">"v1"</span>)
    .appendPath(<span class="hljs-string">"merchant"</span>)
    .appendPath(<span class="hljs-string">"pay"</span>)
    .appendQueryParameter(<span class="hljs-string">"reference_id"</span>, <span class="hljs-string">"MG1489414003"</span>)
    .appendQueryParameter(<span class="hljs-string">"merchant_id"</span>, <span class="hljs-string">"3"</span>)
    .appendQueryParameter(<span class="hljs-string">"product_key"</span>, <span class="hljs-string">"25ab5370735e20391ce2df6329df5c1d9fa15c41"</span>)
    .appendQueryParameter(<span class="hljs-string">"uuid"</span>, <span class="hljs-string">"456789876543456796gvbndcvbnmnbv"</span>)
    .appendQueryParameter(<span class="hljs-string">"amount"</span>, <span class="hljs-string">"20000"</span>)
    .appendQueryParameter(<span class="hljs-string">"description"</span>, <span class="hljs-string">"Buy MaliyoToken5000 for 200 NGN"</span>);
    
String paymentUrl = builder.build().toString();

WebView webView = (WebView) findViewById(R.id.webView);
webView.loadUrl(paymentUrl);

</code></pre>
                            <h5 id="2_iOS_ObjectiveC_62" class="scrollspy">2. iOS Objective-C</h5>
                            <p>Introduced in iOS8 and OS X 10.10 is NSURLQueryItem, which can be used to build queries. You can also read more on this link <a href="http://stackoverflow.com/questions/718429/creating-url-query-parameters-from-nsdictionary-objects-in-objectivec">Creating URL query parameters from NSDictionary objects in ObjectiveC</a></p>
                            <pre><code class="language-obj-c wrap"><span class="hljs-built_in">NSURLComponents</span> *components = [<span class="hljs-built_in">NSURLComponents</span> componentsWithString:<span class="hljs-string">@"http://beta.monapay.com:80/v1/merchant/pay"</span>];
<span class="hljs-built_in">NSURLQueryItem</span> *reference_id = [<span class="hljs-built_in">NSURLQueryItem</span> queryItemWithName:<span class="hljs-string">@"reference_id"</span> value:<span class="hljs-string">@"MG1489414003"</span>];
<span class="hljs-built_in">NSURLQueryItem</span> *merchant_id = [<span class="hljs-built_in">NSURLQueryItem</span> queryItemWithName:<span class="hljs-string">@"merchant_id"</span> value:<span class="hljs-string">@"3"</span>];
<span class="hljs-built_in">NSURLQueryItem</span> *product_key = [<span class="hljs-built_in">NSURLQueryItem</span> queryItemWithName:<span class="hljs-string">@"product_key"</span> value:<span class="hljs-string">@"25ab5370735e20391ce2df6329df5c1d9fa15c41"</span>];
<span class="hljs-built_in">NSURLQueryItem</span> *uuid = [<span class="hljs-built_in">NSURLQueryItem</span> queryItemWithName:<span class="hljs-string">@"uuid"</span> value:<span class="hljs-string">@"456789876543456796gvbndcvbnmnbv"</span>];
<span class="hljs-built_in">NSURLQueryItem</span> *amount = [<span class="hljs-built_in">NSURLQueryItem</span> queryItemWithName:<span class="hljs-string">@"amount"</span> value:<span class="hljs-string">@"20000"</span>];
<span class="hljs-built_in">NSURLQueryItem</span> *description = [<span class="hljs-built_in">NSURLQueryItem</span> queryItemWithName:<span class="hljs-string">@"description"</span> value:<span class="hljs-string">@"Buy MaliyoToken5000 for 200 NGN"</span>];
components<span class="hljs-variable">.queryItems</span> = @[ reference_id, merchant_id, product_key, uuid, amount, description ];
<span class="hljs-built_in">NSURL</span> *paymentUrl = components<span class="hljs-variable">.URL</span>; 

[<span class="hljs-keyword">self</span><span class="hljs-variable">.webView</span> loadRequest:[<span class="hljs-built_in">NSURLRequest</span> requestWithURL:paymentUrl]];
</code></pre>
                            <h5 id="3_PHP_for_web_79" class="scrollspy">3. PHP for web</h5>
                            <p>You can easily use our cross platform payment solution for your web application, here is a simple howto in PHP.</p>
                            <pre><code class="language-php wrap"><span class="hljs-preprocessor">&lt;?php</span>
<span class="hljs-variable">$referenceId</span> = <span class="hljs-string">"MG"</span> . time();
<span class="hljs-variable">$merchantId</span> = <span class="hljs-string">"3"</span>;
<span class="hljs-variable">$productKey</span> = <span class="hljs-string">"25ab5370735e20391ce2df6329df5c1d9fa15c41"</span>;
<span class="hljs-variable">$deviceUuid</span> = <span class="hljs-string">"456789876543456796gvbndcvbnmnbv"</span>;
<span class="hljs-variable">$amount</span> = <span class="hljs-number">20000</span>;
<span class="hljs-variable">$description</span> = <span class="hljs-string">"Buy MaliyoToken5000 for 200 NGN"</span>;

<span class="hljs-variable">$data</span> = <span class="hljs-keyword">array</span>(
    <span class="hljs-string">"reference_id"</span> =&gt; <span class="hljs-variable">$referenceId</span>,
    <span class="hljs-string">"merchant_id"</span> =&gt; <span class="hljs-variable">$merchantId</span>,
    <span class="hljs-string">"product_key"</span> =&gt; <span class="hljs-variable">$productKey</span>,
    <span class="hljs-string">"uuid"</span> =&gt; <span class="hljs-variable">$deviceUuid</span>,
    <span class="hljs-string">"amount"</span> =&gt; <span class="hljs-variable">$amount</span>,
    <span class="hljs-string">"description"</span> =&gt; <span class="hljs-variable">$description</span>
);

<span class="hljs-variable">$paymentUrl</span> = <span class="hljs-string">"http://beta.monapay.com:80/v1/merchant/pay?"</span> . http_build_query(<span class="hljs-variable">$data</span>);

header(<span class="hljs-string">"Location: $paymentUrl"</span>);

</code></pre>
                            <h4 id="2_Payment_Verification" class="scrollspy"><br>2. Payment Verification</h4>
                            <p>Immediately after closing the popup browser(web) or in-app browser (native), the developer will make an HTTP GET request with the transaction reference id to fetch the status of the transaction.</p>
                            <h5>
                                <a id="Initialization_endpoint_109"></a>Initialization endpoint:</h5>
                            <pre><code class="language-sh wrap"><span class="hljs-comment"># http://beta.monapay.com:80/v1/merchant/verifytransaction/233000046342388898765</span>
</code></pre>
                            <h5>HTTP Request</h5>
                            <pre><code class="language-sh wrap"><span class="hljs-attribute">GET</span> <span class="hljs-value"><span class="hljs-string">/v1/merchant/verifytransaction/233000046342388898765 HTTP/1.1</span></span>
<span class="hljs-attribute">Host</span>: <span class="hljs-value"><span class="hljs-string">beta.monapay.com:80</span></span>
<span class="hljs-attribute">Content-Type</span>: <span class="hljs-value"><span class="hljs-string">application/json</span></span>
<span class="hljs-attribute">Ocm-Spmi</span>: <span class="hljs-value"><span class="hljs-string">Y5MC</span></span>
<span class="hljs-attribute">Authentication</span>: <span class="hljs-value"><span class="hljs-string">Basic a61c4c0873d3a94a8fe5ccb2fbbd319b91e98798</span></span>
<span class="hljs-attribute">Cache-Control</span>: <span class="hljs-value"><span class="hljs-string">no-cache</span></span>
</code></pre> [Authentication] - Basic + <span class="red-text">the product key</span>
                            <br><br><br><br>
                            <h5>Likely response [success]</h5>
                            <pre><code class="language-json wrap">{
    "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-string">"success"</span></span>,
    "<span class="hljs-attribute">data</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-string">"1009"</span></span>,
        "<span class="hljs-attribute">reference_id</span>": <span class="hljs-value"><span class="hljs-string">"233000046342388898765"</span></span>,
        "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-string">"299"</span></span>,
        "<span class="hljs-attribute">temp_uuid</span>": <span class="hljs-value"><span class="hljs-string">"456789876543456796gvbndcvbnmnbv"</span></span>,
        "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span class="hljs-string">"payment"</span></span>,
        "<span class="hljs-attribute">is_credit</span>": <span class="hljs-value"><span class="hljs-string">"0"</span></span>,
        "<span class="hljs-attribute">product_key</span>": <span class="hljs-value"><span class="hljs-string">"a61c4c0873d3a94a8fe5ccb2fbbd319b91e98798"</span></span>,
        "<span class="hljs-attribute">amount</span>": <span class="hljs-value"><span class="hljs-string">"145000"</span></span>,
        "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"transaction payment for E-Commerce transaction"</span></span>,
        "<span class="hljs-attribute">confirmed</span>": <span class="hljs-value"><span class="hljs-string">"1"</span></span>,
        "<span class="hljs-attribute">confirmed_time</span>": <span class="hljs-value"><span class="hljs-string">"2017-05-17 12:22:04"</span></span>,
        "<span class="hljs-attribute">successful</span>": <span class="hljs-value"><span class="hljs-string">"1"</span></span>,
        "<span class="hljs-attribute">condition</span>": <span class="hljs-value"><span class="hljs-string">"Successful"</span></span>,
        "<span class="hljs-attribute">created</span>": <span class="hljs-value"><span class="hljs-string">"2016-12-19 16:07:44"</span></span>,
        "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-string">"1"</span></span>,
        "<span class="hljs-attribute">full_condition</span>": <span class="hljs-value"><span class="hljs-string">"₦1450 being transaction payment for E-Commerce transaction : Successful. Balance: ₦0"</span></span>,
        "<span class="hljs-attribute">current_balance</span>": <span class="hljs-value"><span class="hljs-number">0</span>
    </span>}
</span>}
</code></pre>

                            <h5>Likely error</h5>
                            <pre><code class="language-json">{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-string">"error"</span></span>,
  "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"Reference ID already exist"</span></span>,
  "<span class="hljs-attribute">code</span>": <span class="hljs-value"><span class="hljs-string">"E0005"</span>
</span>}
</code></pre>
                            <ul>
                                <li>[status] - status of <code>error</code> for all error reponse</li>
                                <li>[message] - reason for the error</li>
                                <li>[code] - internal error code</li>
                            </ul>
                            <br>
                            <p>For further enquiries please contact the admin.</p>

                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <?php include("footer.php"); ?>