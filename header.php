<?php
$mainpath = "/";
?>
<!doctype html>
<html lang="en">

<head>
    <!-- COMMON TAGS -->
    <meta charset="utf-8">
    <title>Monapay - Instant Airtime Payment Solution for Nigerians</title>
    <!-- Search Engine -->
    <meta name="description" content="Monapay enables you to monetize your products using airtime payments. Users are 10x more likely to use airtime to pay for digital goods. Sign up now.">
    <meta name="image" content="https://monapay.com/img/opengraph.png">
    <!-- Schema.org for Google -->
    <meta itemprop="name" content="Monapay - Instant Airtime Payment Solution for Nigerians">
    <meta itemprop="description" content="Monapay enables you to monetize your products using airtime payments. Users are 10x more likely to use airtime to pay for digital goods. Sign up now.">
    <meta itemprop="image" content="https://monapay.com/img/opengraph.png">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="Monapay - Instant Airtime Payment Solution for Nigerians">
    <meta name="twitter:description" content="Monapay enables you to monetize your products using airtime payments. Users are 10x more likely to use airtime to pay for digital goods. Sign up now.">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="Monapay - Instant Airtime Payment Solution for Nigerians">
    <meta name="og:description" content="Monapay enables you to monetize your products using airtime payments. Users are 10x more likely to use airtime to pay for digital goods. Sign up now.">
    <meta name="og:image" content="https://monapay.com/img/opengraph.png">
    <meta name="og:url" content="https://monapay.com">
    <meta name="og:site_name" content="Monapay Site">
    <meta name="og:type" content="website">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/stack-interface.css" rel="stylesheet" type="text/css" media="all">
    <link href="css/socicon.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/flickity.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet" type="text/css">
    <link href="css/font-roboto.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="css/devices.min.css" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="css/index-codepanel.css">
    <link rel="stylesheet" href="css/default-codepanel.css">
    <link rel="stylesheet" href="css/prism_light.css">
    <script defer src="js/default-codepanel.js"></script>
    <script defer src="js/index-codepanel.js"></script>
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="img/Monapay_favicon.png">
    <link rel="icon" type="image/png" href="img/Monapay_favicon.png" sizes="32x32">
</head>
<style type="text/css">
    .hide-it {
        display: none !important;
    }
</style>

<body class="scroll-assist" data-smooth-scroll-offset="77">
    <a id="start"></a>
    <div class="nav-container">
        <div class="via-1491986892298" via="via-1491986892298" vio="monapay">
            <div class="bar bar--sm visible-xs">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-8 col-sm-2">
                            <a href="<?= $mainpath ?>home.php"> <img class="logo logo-dark" alt="logo" src="img/Asset%201monapay%20logo.svg"> <img class="logo logo-light" alt="logo" src="img/Asset%201monapay%20logo.svg"> </a>
                        </div>
                        <div class="col-xs-12 col-sm-5 text-right">
                            <a href="#" class="hamburger-toggle" data-toggle-class="#menu1;hidden-xs hidden-sm"> <i class="material-icons">menu</i> </a>
                        </div>
                    </div>
                </div>
            </div>
            <nav id="menu1" class="bar bar--sm bar-1 hidden-xs" data-scroll-class="10vh:pos-fixed">
                <div class="container">
                    <div class="row">
                        <div class="col-md-2 col-sm-2 hidden-xs header1">
                            <div class="bar__module">
                                <a href="<?= $mainpath ?>home.php"> <img class="logo logo-dark" alt="logo" src="img/Asset%201monapay%20logo.svg"> <img class="logo logo-light" alt="logo" src="img/Asset%201monapay%20logo.svg"> </a>
                            </div>
                        </div>
                        <div class="col-md-11 col-sm-12 text-right text-left-xs text-left-sm header2">
                            <div class="bar__module">
                                <ul class="menu-horizontal text-left">
                                    <li> <a href="<?= $mainpath ?>home.php#how-it-works" class="inner-link" target="_self">
                                        How it Works
                                    </a> </li>
                                    <li> <a href="<?= $mainpath ?>home.php#features" class="inner-link" target="_self">
                                        Features
                                    </a> </li>
                                    <li> <a href="<?= $mainpath ?>home.php#developers" class="inner-link" target="_self">Developers</a>
                                    </li>
                                    <li> <a href="http://www.maliyo.com">
                                      Marketplace
                                    </a> </li>
                                </ul>
                            </div>
<div class="bar__module">
    <div class="modal-instance sgn-up">
        <a class="btn btn--sm modal-trigger sign__up" href="#"> <span class="btn__text">
                            Demo Account
                        </span> </a>
        <div class="modal-container modal" data-modal-index="4">
            <div class="modal-content">
                <section class="unpad ">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6 col-md-offset-3 col-sm-offset-3 col-xs-12">
                                <div class="feature feature-1">
                                    <div class="feature__body boxed boxed--lg boxed--border text-center">
                                        <div class="modal-close modal-close-cross"></div>
                                        <!-- Demo Account form --->
                                        <div>
                                            <div class="text-block">
                                                <h3>Request for demo account</h3>
                                                <p>
                                                    As a developer, you can request for demo account and testing parameters.
                                                </p>
                                            </div>
                                            <?php include ("requestform.phtml"); ?>
                                            <!-- end of demo account form -->
                                            <!-- flip forms 
                                                                    <div id="login">
                                                                        <div class="text-block">
                                                                            <h3>Login to your account</h3>
                                                                            <p>
                                                                                Welcome back, sign in with your existing account credentials.
                                                                            </p>
                                                                        </div>
                                                                        <form>
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <input type="email" name="Email Address" placeholder="Email Address">
                                                                                </div>
                                                                                <div class="col-sm-12">
                                                                                    <input type="password" placeholder="Password">
                                                                                </div>
                                                                                <div class="col-sm-12">
                                                                                    <button class="btn btn--primary type--uppercase" type="submit"><span class="btn__text">Login</span></button>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                        </form>
                                                                        <span class="type--fine-print block">Don't have an account yet? <a href="#" id="flip-login">Create account</a></span> <span class="type--fine-print block">Forgot your username or password? <a href="#" id="flip-recover">Recover account</a></span>
                                                                    </div>
                                                                    
                                                                    <div class="hidden" id="recover-account">
                                                                        <div class="row">
                                                                            <div class="text-block">
                                                                                <h3>Recover your account</h3>
                                                                                <p> Enter email address to send a recovery email. </p>
                                                                                <form>
                                                                                    <div class="col-sm-12">
                                                                                        <input type="email" placeholder="Email Address"></div>
                                                                                    <div class="col-sm-12">
                                                                                        <button class="btn btn--primary type--uppercase" type="submit"><span class="btn__text">Recover Account</span></button>
                                                                                    </div>
                                                                                </form>
                                                                                <span class="type--fine-print block">Dont have an account yet? <a href="#" id="flip-signup-recover">Create account</a></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <div class="hidden" id="signup">
                                                                        <div class="row">
                                                                            <div class="text-block">
                                                                                <h3>Sign up and start earning with monapay</h3>
                                                                                <p>Get started with monapay today, No credit card required.</p>
                                                                                <form>
                                                                                    <div class="row">
                                                                                        <div class="col-sm-12"> <input type="email" name="Email Address" placeholder="Email Address"> </div>
                                                                                        <div class="col-sm-12"> <input type="password" name="Password" placeholder="Password"> </div>
                                                                                        <div class="col-sm-12"> <button type="submit" class="btn btn--primary type--uppercase"><span class="btn__text">Create Account</span></button> </div>
                                                                                        <div class="col-sm-12"> <span class="type--fine-print">By signing up, you agree to the <a href="terms-of-service.html">Terms of Service</a></span> </div>
                                                                                    </div>
                                                                                </form>
                                                                                <span class="type--fine-print block">Already a user? <a href="#" id="flip-signup">Login</a></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>-->

                                        </div>
                                    </div>
                                    <!--end feature-->
                                </div>
                            </div>
                            <!--end of row-->
                        </div>
                        <!--end of container-->
                </section>
                </div>
            </div>
        </div>
    </div>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>
    <div class="main-container">