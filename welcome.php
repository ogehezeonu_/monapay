<!DOCTYPE html>
<html lang="en">
<head>
    <title>monapay</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <link rel="shortcut icon" href="img/" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="css/style-webapp.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet" type="text/css"> 
</head>
<body>
	<div class="container">
	  <div class="content-holder outer">
	  	<div class="content text-center middle">
	  		<div class="logo inner">
		        <div class="logo-container">
		            <a href="#">
						<img class="" src="img/Logo.png" alt="MonaPay">
					</a>
		        </div>
		    </div>
		    <div class="text-content">
		    <center>
		    
		        <h3 class="dark-text break">Welcome to monapay</h3>

		        </center>

		    	<h4 class="dark-text">Select Your Transaction</h4>
		    
				<a href="<?= $mainpath ?>product_info.php"><button class="button colored-button">Make Payment</button></a>
				<p class="dark-text smalll-font">OR</p>
				<a href="<?= $mainpath ?>top_product_info.php"><button class="button stroked-button">Top up</button></a>
		    </div>
	  	</div>
	  </div>
	</div>
</body>
</html> 